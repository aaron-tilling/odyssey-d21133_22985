/*----------------------------------------------------------------------------\
|  Src File:   proc.c                                                         |
|  Version:    1.63                                                           |
|  Authored:   09/15/93, ???                                                  |
|  Function:   Card processing.                                               |
|  Comments:   when this module was started the process functions returned    |
|              1 if OK, change this to return 0 if OK and some none zero      |
|              error code if error(s) occur.                                  |
|                                                                             |
|  $Log: proc.c,v $                                                           |
|      1.00:   07/15/07, aat  -  Module taken from TRiM and modified for use  |
|                                in Farebox when the Farebox is in "Host Mode"|
|                                The functions were designed around the "New" |
|                                magnetic structure that was developed for    |
|                                Cleveland to handle embedded transfers.      |
|                                                                             |
|          Copyright (c) 1994 - 2007 GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
/* System headers
*/
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdarg.h>

/* Project headers
*/
#include "gen.h"
#include "timers.h"
#include "regs.h"
#include "led.h"
#include "port.h"
#include "status.h"
#include "sound.h"
#include "transact.h"
#include "magcard.h"
#include "magcon.h"
#include "mag.h"
#include "tests.h"
#include "menu.h"
#include "pbq.h"
#include "msgs.h"
#include "cnf.h"
#include "display.h"
#include "fare.h"
#include "fbd.h"
#include "data.h"
#include "misc.h"
#include "lcddisp.h"
#include "lamp.h"
#include "process.h"
#include "ocu.h"
#include "scard.h"
#include "dll.h"
#include "trim.h"
#include "rtc.h"
#include "util.h"
#include "text.h"
#include "ability.h"

/* Define(s)
*/
#undef DEBUG

#ifdef DEBUG
   #define  ABORT_TIME  SECONDS( 5 )
#else
   #define  ABORT_TIME  SECONDS( 300 )
#endif


/* Global Data
*/


/* Module data
*/
static   char        Text[9];
static   int         Eject;
static   int         Resp;
static   char  Line[] = 
               "\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07";
static   ulong    Validity;

int      EmbedChk;
int      OriginalGroup;
word     ExpOff;
time_t   ShortExp;


/* Prototypes
*/
int   Period               ( CARD *card );
int   StoredRide           ( CARD *card );
int   Transfer             ( CARD *card );
/*
int   PrintThermometer     ( int ThermoType, int Row, ulong Value, word NewVal );
*/

ulong  ProcessCard( CARD *card )
{
/*  word        validate;               /* read verification status */
  int         Fix_card;

  Init_Tmr_Fn( AUTO_LOG_TMR, AutoDriverLogOff, SECONDS(AUTO_LOGOFF_TM) ); /* start full fare default timer */  
  Init_Tmr_Fn( AUTO_DUMP_TMR, AutoDump, AutoDumpTime );

  EmbedChk = Fix_card = FALSE;
  Validity = 0L; 

  Clr_Pflag( P_ESCROW );
  Set_Pflag( P_CARDPRESENT );
  Set_Pflag( P_PROCESSING );

  if ( !MachineOK() )                      /* check if fb out of service */
  {
    if( Tst_Sflag( S_DOOR|S_CASHBOX ) ) /* if door or no cashbox toss card */
      goto function_end;                  /* ignore card if out of service */
  }
  if ( Tst_Pflag(P_PROCESSING|P_CARDPRESENT) && Tst_Pflag( P_RECHARGE ) )
    goto function_end;           /* ignore card if TRIM card recharge */

  if ( tmr_expired(IDLE_MISC_TMR) || Mode )
  {
    if( Mode == MENU_ENTRY || ( Mode == DATA_DISPLAY && 
      Screen == READ_CARD_SCREEN && Tst_Pflag(P_DISPLAY_CARD ) ) )
    {
    }
    else
      DefaultDisplay();
  }
  else
    Init_Tmr_Fn( IDLE_MISC_TMR, BlankDisplay, SECONDS( 60 ) ); /* start hibernation  timer */  

  Mlist.Cumulative.TrimReads++;
  Mlist.Maintenance.TrimReads++;
  Ml.pass_t++;
  Ml.pass_c++;

  if ( Mode == MENU_ENTRY && ( TrimCard.Group != MAINT_EMP_MG || 
    TrimCard.Desig != DRIVER_ID ))
  {
    DefaultDisplay();
  }

  if( (Screen == READ_CARD_SCREEN) && OF1_OCU_MENUS && Tst_Pflag( P_DISPLAY_CARD ))
  {
    Fix_card = 0;
    Clr_Pflag( P_RECHARGE|P_RESTORE_SC|P_SC_PEND_RECHARG|P_SC_RECHARGE );
    goto function_end;
  }

  MagFormat = TRIMCARDS;
  TrimCard.MagCardFlags = 0;

   switch( TrimCard.Group )
   {
   case PERIOD_MG:
      if( MachineOK() )
      {
         if( BadFlagSet )
         {
            BadFlagSet = FALSE;
            Validity |= BAD;
         }

         if( Period( &TrimCard ) )
            ProcessErrors( &TrimCard, Validity );
         else
         {
/** AAT - REMOVE **/
            printf( "Period Pass CurrentFare: %d\n", CurrentFare );
         }
      }
      break;

   case S_RIDE_MG:
      if( MachineOK() )
      {
         if( BadFlagSet )
         {
            BadFlagSet = FALSE;
            Validity |= BAD;
         }

         EmbedChk = TRUE;

         if( Transfer( &TrimCard ) )
         {
            if ( (Validity & (PBK|BXFER)) == 0 )
            {
              Validity = 0L;
              if( StoredRide( &TrimCard ) )
                 ProcessErrors( &TrimCard, Validity );
              else
              {
/** AAT - REMOVE **/
                 printf( "Ride Card CurrentFare: %d\n", CurrentFare );
              }
            }
            else
            {
/** AAT - REMOVE **/
               printf( "Ride/transfer error: %08lX\n", Validity );
               ProcessErrors( &TrimCard, Validity );
            }
         }
         else
         {
/** AAT - REMOVE **/
              printf( "Ride Card embedder xfer CurrentFare: %d\n", CurrentFare );
         }
      }
      break;

/*
  case MAINT_EMP_MG:
    if( Maintenance( &TrimCard ) )
      ProcessErrors( &TrimCard, Validity );
    break;

  case S_VALUE_MG:
    if( MachineOK() )
    {
      if( BadFlagSet )
      {
        BadFlagSet = FALSE;
        Validity |= BAD;
      }

      if( StoredValue( &TrimCard ) )
        ProcessErrors( &TrimCard, Validity );
    }
    break;
*/

   case TRANSFER_MG:
      if( MachineOK() )
      {
         if( BadFlagSet )
         {
            BadFlagSet = FALSE;
            Validity |= BAD;
         }

         if( Transfer( &TrimCard ) )
            ProcessErrors( &TrimCard, Validity );
         else
         {
/** AAT - REMOVE **/
            printf( "Transfer CurrentFare: %d\n", CurrentFare );
         }
      }
      break;

  default:
    if( MachineOK() )
       InvalidCard();
    break;
  }

  function_end:          

  Init_Tmr_Fn( FF_DEFAULT_TMR, FullFareDefault, SECONDS( 30 ) ); /* start full fare default timer */  
  Init_Tmr_Fn( AUTO_DUMP_TMR, AutoDump, AutoDumpTime );

  return Validity;
}


static
int   Period( CARD *card )
{
   int         E = -1,j;
   int         AgencyID;
   BFARE        *u;

/** AAT - REMOVE **/
   printf( "int   Period( CARD *card )\n" );

   j = ( card->Group<<5 | card->Desig&0x1f );          

   card->TTP_used = scan_media_table( j );

   if ( (int)card->TTP_used == -1 )
   {
     Validity |= MLE;
     goto function_end;
   }

   if ( card->AgencyID == 0 )
      AgencyID = -1;
   else
      AgencyID  = scan_agencies( card );                    
   
/* AAT - PASSBACK */
   if ( !Tst_Sflag( S_SPARES1 ))
   {
     Validity |= ( check_pbq( (B_SERIAL*)card->serial ) == NOT_FOUND )  ? 0 : PBK;

     if( Validity == (Validity & PBK) && Tst_Aflag( A_DIS_PASSBACK ) )
     {
       Mlist.System.PassBack++;
       Ml.pass_e[1]++;
       Validity &= ~PBK;
       card->spare1 = 0x01;
     }
     Clr_Aflag( A_DIS_PASSBACK );
   }

   Validity |= ( AgencyID == -1 )                    ? AGLE : 0;
   Validity |= ( isgood_security( card->Security ) ) ? 0 : SCV; 
   Validity |= ( isbadlisted( card, (B_SERIAL*)card->serial ) ) ? BAD : 0; 
   Validity |= ( scan_bigbad_list( (B_SERIAL*)card->serial ) )  ? BAD : 0; 
   Validity |= is_sc_date_ok( card );                             
   Validity |= check_card_restrictions( card->TTP_used );       

   if ( Validity == 0L )
   {
      card->TTP_used += FE; 
      card->KTFare = card->TTP_used; 
      u = &Dlist.FareTbl[FbxCnf.fareset][ card->TTP_used+1 ]; /* fetch fare value */ 

      if( !u->attrib || u->attrib == TallyClear || u->attrib == HoldCard || 
           u->attrib == TallyDumpClear || u->attrib == TallyClrBlank ||
           ( u->attrib >= FREE_FARE && u->attrib <= FREE_BLANK_FARE ) )
      { 
         PreSound( u );
         card->FareCell = u->fares;     

         if ( card->FirstUse )
         {  
            card->MagCardFlags |= (PRNT|ENCD);

            FareSatisfied = TRUE;
            if ( card->FirstUse != 1 )      /* first or 2nd use */
               card->FirstUse = 0;           /* 2nd, clear flag  */
         }

         disp_card_text( card );
         MagFormat = TRIMCARDS;

         CashDisplay( Driver_Rev );
         CurrentFare = card->KTFare;
         QueuePass   = TRUE;          /* set pass pending flag for pbq */
      }
      else
      {
         Validity |= PED;
         Unused_Card();
         Clr_Pflag( P_PROCESSING );
      }
      E = 0;
   }     

   function_end:

   if ( E || Validity != 0L )
   {
     E = 1;
     Clr_Pflag( P_PROCESSING );
   }

   return E;
}


static
int   StoredRide( CARD *card )
{
   int         E = -1,j;
   int         AgencyID;
   BFARE      *u;


/** AAT - REMOVE **/
   printf( "int   StoredRide( CARD *card )\n" );

   j = ( card->Group<<5 | card->Desig&0x1f );          

   card->TTP_used = scan_media_table( j );

   if ( (int)card->TTP_used == -1 )
   {
     Validity |= MLE;
     goto function_end;
   }

   if ( card->AgencyID == 0 )
      AgencyID = -1;
   else
      AgencyID  = scan_agencies( card );                    
   
/** AAT - REMOVE **
   printf( "\nAgencyID: %d\n",card->AgencyID );
   printf( "Desig: %d,  TTP %d,   PrintFormat: %d,   ",
            card->Desig, card->TTP_used, card->PrintFormat );
   printf( "PrintPos: %d,  RemVal: %d\n",
            card->PrintPos, card->RemVal );
   printf( "Route: %06lu,  Dir: %c\n\n",
            FbxCnf.route, DirLet[FbxCnf.dir] );

*/
   Validity |= ( AgencyID == -1 )                    ? AGLE : 0;
   Validity |= ( isgood_security( card->Security ) ) ? 0 : SCV; 
   Validity |= ( isbadlisted( card, (B_SERIAL*)card->serial ) ) ? BAD : 0; 
   Validity |= ( scan_bigbad_list( (B_SERIAL*)card->serial ) )  ? BAD : 0; 
   Validity |= is_sc_date_ok( card );                             
   Validity |= check_card_restrictions( card->TTP_used );       
   Validity |= card->RemVal == 0 ? NRR : 0;

/** AAT - REMOVE **/
   printf( "Validity: %8lX\n", Validity );

   if ( Validity == 0L )
   {
      card->TTP_used += FE; 
      card->KTFare = card->TTP_used; 

      u = &Dlist.FareTbl[FbxCnf.fareset][ card->TTP_used+1 ]; /* fetch fare value */ 

      if( !u->attrib || u->attrib == TallyClear || u->attrib == HoldCard ||
           u->attrib == TallyDumpClear || u->attrib == TallyClrBlank ||
           ( u->attrib >= FREE_FARE && u->attrib <= FREE_BLANK_FARE ) )
      { 
         PreSound( u );

         card->FareCell = u->fares;     

         card->RemVal--;

         card->MagCardFlags = (PRNT|ENCD);

         FareSatisfied = TRUE;
         if ( card->FirstUse != 1 )      /* first or 2nd use */
            card->FirstUse = 0;           /* 2nd, clear flag  */

         disp_card_text( card );
         MagFormat = TRIMCARDS;
         CashDisplay( Driver_Rev );
         CurrentFare = card->KTFare;
      }
      E = 0;
   }

   if ( E || Validity != 0L )
   {
     E = 1;
     Clr_Pflag( P_PROCESSING );
   }

   function_end:
   return E;
}


static
int  StoredValue()
{
}

static
int  Maintenance()
{
}

static
int  Transfer( CARD *card )
{
   int         E = -1,j,trips;
   int         AgencyID;
/*   time_t      now = time( (time_t*)NULL ); */
   BFARE       *u;


/** AAT - REMOVE **/
   printf( "\n -- int   Transfer( CARD *card )\n" );

   j = ( card->XferDesig&0x1f );

   card->TTP_used = scan_media_table( j );

   if ( card->TTP_used == -1 )
   {
      Validity |= MLE;
      goto function_end;
   }

   card->TCIndex = scan_tc_table( card );
  
   if ( card->TCIndex == -1 )
   {
      Validity |= TCE;
      goto function_end;
   }

   if ( card->AgencyID == 0 )
      AgencyID = -1;
   else
      AgencyID  = scan_agencies( card );                    
   
/* AAT - PASSBACK */
   if ( !Tst_Sflag( S_SPARES1 ))
   {
     Validity |= ( check_pbq( (B_SERIAL*)card->serial ) == NOT_FOUND )  ? 0 : PBK;

     if( Validity == (Validity & PBK) && Tst_Aflag( A_DIS_PASSBACK ) )
     {
       Mlist.System.PassBack++;
       Ml.pass_e[1]++;
       Validity &= ~PBK;
       card->spare1 = 0x01;
     }
     Clr_Aflag( A_DIS_PASSBACK );
   }

   trips  = Dlist.TransferControl[card->TCIndex].NumTrips;
   ExpOff = Dlist.TransferControl[card->TCIndex].ExpOff;

   Validity |= ( AgencyID == -1 )                    ? AGLE : 0;
   Validity |= ( isgood_security( card->Security ) ) ? 0 : SCV; 

/*
   if( OF5_REVTRP_HLF_XFEREXP && (FbxCnf.route == card->IURoute) &&
     ((FbxCnf.dir + card->IUDir) == 5) )
   {
     Validity |= (card->XferExp-(ExpireOffset/2)) > (now) ? 0 : TEX;
     printf("use half time\n");
   }
   else
     Validity |= card->XferExp > (now) ? 0 : TEX;
*/
   Validity |= CheckEmbedded( card );
   Validity |= check_card_restrictions( card->TTP_used );       
   Validity |= check_xfer_restrictions( card->TCIndex, card );
   Validity |= (trips && (card->XferTrips >= trips)) ? TNT : 0;

   if ( ((Validity & TPB) == TPB ) || ((Validity & TNT) == TNT ) )
   {
     if ( OverRideKey  )
     {
        OverRideKey = FALSE;              /* clear override */
        Validity = Validity & 0x010900ff; /* see magcard.h for bit map def */
     } 
   } 

   if ( Validity == 0L )
   {
      card->TTP_used += FE; 
      card->KTFare = card->TTP_used; 

      u = &Dlist.FareTbl[FbxCnf.fareset][ card->TTP_used+1 ]; /* fetch fare value */ 

      if( !u->attrib || u->attrib == TallyClear || u->attrib == HoldCard ||
          u->attrib == TallyDumpClear || u->attrib == TallyClrBlank ||
          ( u->attrib >= FREE_FARE && u->attrib <= FREE_BLANK_FARE ) )
      { 
         PreSound( u );
         card->FareCell = u->fares;
         FareSatisfied = TRUE;
         CurrentFare = card->KTFare;
         MagFormat = TRIMCARDS;;
         disp_card_text( card );
         CashDisplay( Driver_Rev );
         QueuePass = TRUE;          /* set pass pending flag for pbq */
         if( Driver_Rev < u->fares ) 
         {
         } 
         else
            Clr_Pflag( P_FAREHOLD); 
      }
      else
      {
         if ( EmbedChk )
         {
           Validity |= TDIS;
           card->XferExp = 0;    /* clear time */
         }
         else
         {
           Validity |= PED;
           Unused_Card();
         }
      }
      E = 0;
   }     

   function_end:
   if ( E || Validity != 0L )
   {
     E = 1;
     if ( !EmbedChk )
       Clr_Pflag( P_PROCESSING );
   }
   EmbedChk = FALSE;

/** AAT - REMOVE **/
   printf(" Transfer Validity %8lX  error %d\n", Validity, E );
 
   return E;
}


ulong  CheckEmbedded( CARD *card )
{
   time_t      now = time( (time_t*)NULL );
   struct tm  *c = gmtime( &now );
   int         expire = 0;
   ulong       rc = 0; 
   char        s[17];

/** AAT - REMOVE **/
   c = gmtime( &now );
   s[0] = '\0';
   strftime( s, sizeof(s)-1, "%d%b%y %I:%M",c );
   printf( "\nNow Data:\n" );
   printf( "            NowTm: %s  ( %ld sec )\n", s, now );
   printf( "            Route: %06lu\n",FbxCnf.route );
   printf( "            Dir  : %c\n",(DirLet[FbxCnf.dir]));

   c = gmtime( &card->XferExp );
   s[0] = '\0';
   strftime( s, sizeof(s)-1, "%d%b%y %I:%M",c );
   printf( "Card Data:\n" );
   printf( "            ExpTm: %s  ( %ld sec )\n", s, card->XferExp );
   printf( "            Route: %06lu\n",card->IURoute );
   printf( "            Dir  : %c\n",(DirLet[card->IUDir]));


   if ( card->XferExp < now )
   {
     rc |= TEX;
     ++expire;
/** AAT - REMOVE **/
     printf("use full time %lu \n", card->XferExp );
   }
   else if ( OF5_REVTRP_HLF_XFEREXP )
   {
     if ( (FbxCnf.route == card->IURoute) && ((FbxCnf.dir + card->IUDir) == 5) )
     {
       ComputeXferExp( Dlist.TransferControl[card->TCIndex].ExpOff );
       if ( (card->XferExp-(ExpireOffset/2)) > now ? FALSE : TRUE )
       {
/** AAT - REMOVE **/
         printf("half expired\n"); 
         if( (card->RemVal == 0 && EmbedChk) || !EmbedChk)
         {
           rc |= (TODSR|BXFER);
/** AAT - REMOVE **/
           printf("expired embedded xfer\n");
         }
         else 
         {
           rc |= TEX;
           ++expire;
/** AAT - REMOVE **/
           printf("expired xfer\n");
         }
       }
     }
   }

/** AAT - FIX ME !!! **/
   if( ( (card->RemVal == 0 && EmbedChk ) || !EmbedChk ) && 
     card->XferExp && expire )
   {
      OriginalGroup = card->Group;
      card->Group = TRANSFER_MG;
      PrintCard( card );
      card->Group = OriginalGroup;
      card->XferExp = 0L;
      PutCard( &TrimCard );
      TrimCflags |= CARD_POS_2_WRITE;
      TrimCflags |= CARD_WRITE;
      TrimEncodeData( &MagStripeData, REVERSE_DATA, 0 );
   }
   return rc;
}



int   PrintCard( CARD *card )
{
   int         E = -1, PrintFlag = 0;
   int         trips;
   time_t      now   = time( (time_t*)NULL );
   struct tm  *c = gmtime( &now );
   char        s[17];
   

   TrimCmd( TRIMCLRPRINTBUF, NULL, 0 );

   switch( card->Group )
   {
   case PERIOD_MG:
      if( card->FirstUse )
      {
         PrintFlag = 1;

         if( card->PrintFormat == 0 || card->PrintFormat > 5 )
            card->PrintPos = 3;     /* Defaults to large issue date */
                                    /* in the middle                */

         if( card->PrintFormat == 5 )
         {
            tprintf( card->PBase, card->PrintPos, "FIRST USE:%5lu", Mlist.Config.BusNum );
            card->PrintPos += 4; 

            s[0] = '\0';
            strftime( s, sizeof(s)-1, "%b %d - %I:%M", c );
            s[1] = toupper( s[1] );
            s[2] = toupper( s[2] );
            tprintf( card->PBase, card->PrintPos,"%s%c", s, (c->tm_hour<12) ? 2:3 );


            tprintf( card->PBase, card->PrintPos+6,"EXPIRES:" );
            card->PrintPos += 9;

            now = card->Expiration;

            if ( (now % DaySec) == 0 )
            now -= DaySec;                    /* now = last day pass is good */
            c = gmtime( &now );

            s[0] = '\0';
            strftime( s, sizeof(s)-1, "%a", c );
            s[1] = toupper( s[1] );
            s[2] = toupper( s[2] );
            tprintf( card->PBase, card->PrintPos,"\\B--%s--", s );
            card->PrintPos += 6;

            s[0] = '\0';
            strftime( s, sizeof(s)-1, "%d", c );
            tprintf( card->PBase, card->PrintPos,"\\B%s", s );
     
            s[0] = '\0';
            strftime( s, sizeof(s)-1, "%b", c );
            s[1] = toupper( s[1] );
            s[2] = toupper( s[2] );
            tprintf( card->PBase+9, card->PrintPos,"\\B%s", s );

            s[0] = '\0';
            strftime( s, sizeof(s)-1, "%y", c );
            tprintf( card->PBase+22, card->PrintPos, "\\B%s", s );

            card->PrintPos += 6;
            tprintf( card->PBase, card->PrintPos+2, "at" );

            if ( (now % DaySec) == 0 )
               tprintf( card->PBase+5, card->PrintPos,"\\B11:59%c", 3);
            else
            {
               s[0] = '\0';
               strftime( s, sizeof(s)-1, "%I:%M", c );
               tprintf( card->PBase+5, card->PrintPos,"\\B%s%c", s, (c->tm_hour<12) ? 2:3 );
            }
         }
         else
         {
            if( card->PrintPos == 4 )              /* Reverse video at top */
            {
               card->PrintPos = 15;
               c->tm_year %= 100;
               tprintf( card->PBase, card->PrintPos, "\\RACTIVE %02d/%02d/%02d",
                           c->tm_mon + 1, c->tm_mday, c->tm_year );
               now = card->Expiration;
               c   = gmtime( &now );
               tprintf( card->PBase, card->PrintPos+3, "\\REXPIRE %02d/%02d/%02d",
                           c->tm_mon + 1, c->tm_mday, (c->tm_year%100) );
               card->PrintPos += 6;
            }
            else
            {
               if( card->PrintPos == 1 )          /* Small issue date in middle */
               {
                  card->PrintPos = 39;        /* init print position */    
				  
                  s[0] = '\0';
                  strftime( s, sizeof(s)-1, "%d %b %Y", c );
                  s[4] = toupper( s[4] );
                  s[5] = toupper( s[5] );
                  tprintf( card->PBase+5, card->PrintPos, "%s", s );
                  now = card->Expiration;
                  c = gmtime( &now );
               }
               else
               {
                  /* Large issue date in middle */
                  /* init print position */    
                  if( card->PrintPos == 3 || card->PrintFormat == 3 )
                  {
                     card->PrintPos = 24;
                  }

                  /*** Defaults to large issue date at the top ***/
                  tprintf( card->PBase, card->PrintPos, "FIRST USE:");
                  card->PrintPos += 3; 

                  s[0] = '\0';
                  strftime( s, sizeof(s)-1, "%d", c );
                  tprintf( card->PBase, card->PrintPos,"\\B%s", s );
				  
                  s[0] = '\0';
                  strftime( s, sizeof(s)-1, "%b", c );
                  s[1] = toupper( s[1] );
                  s[2] = toupper( s[2] );
                  tprintf( card->PBase+9, card->PrintPos,"\\B%s", s );

                  s[0] = '\0';
                  strftime( s, sizeof(s)-1, "%y", c );
                  tprintf( card->PBase+22, card->PrintPos, "\\B%s", s );

                  s[0] = '\0';
                  strftime( s, sizeof(s)-1, "%I:%M", c );
                  tprintf( card->PBase, card->PrintPos+6,"at %s%c", s, (c->tm_hour<12) ? 2:3 );
                  card->PrintPos += 2;

                  tprintf( card->PBase, card->PrintPos+9,"EXPIRES:" );
                  s[0] = '\0';
                  now = card->Expiration;

                  if ( (now % DaySec) == 0 )
                  {
                     now -= DaySec;                    /* now = last day pass is good */
                     c = gmtime( &now );
                     tprintf( card->PBase, card->PrintPos+19,"at 11:59%c", 3);
                  }
                  else
                  {
                     c = gmtime( &now );
                     s[0] = '\0';
                     strftime( s, sizeof(s)-1, "%I:%M", c );
                     tprintf( card->PBase, card->PrintPos+19,"at %s%c", s, (c->tm_hour<12) ? 2:3 );
                  }
               }

               s[0] = '\0';
               strftime( s, sizeof(s)-1, "%d", c );
               tprintf( card->PBase, card->PrintPos+12,"\\B%s", s );
			  
               s[0] = '\0';
               strftime( s, sizeof(s)-1, "%b", c );
               s[1] = toupper( s[1] );
               s[2] = toupper( s[2] );
               tprintf( card->PBase+9, card->PrintPos+12,"\\B%s", s );

               s[0] = '\0';
               strftime( s, sizeof(s)-1, "%y", c );
               tprintf( card->PBase+22, card->PrintPos+12, "\\B%s", s );
            }
         }
      }
      break;

   case S_RIDE_MG:
      if ( (card->PrintFormat == 0) || (card->PrintFormat == 4) )
      {
         if( card->PrintPos == 1 )
         {
            tprintf( card->PBase, card->PrintPos, "\\SRIDES   DATE       TIME" );
            tprintf( card->PBase, card->PrintPos+1, Line );
            card->PrintPos += 4;
         }

         if ( card->PrintFormat != 4 )
         {
            if ( card->RemVal > 29 )
            {
               card->PrintFormat = 1;        /* set direction to West */
               PrintFlag = 1;
            }
            else if ( card->RemVal > 14 ) 
               card->PrintFormat = 2;        /* set direction to East */
            else
               card->PrintFormat = 3;
         }
         else
         {
            if ( card->RemVal > 29 ) 
            {
               card->PrintFormat = 1;           /* set direction to West */
               PrintFlag = 1;
            }
            else if ( card->RemVal > 19 ) 
               card->PrintFormat = 2;
            else
               card->PrintFormat = 3;           /* set direction to South */
         }
      }
      /* End of First Use */
/*
      if( (card->PrintPos > 52) && (card->RemVal > 0) )
      {
         IssueNew = TRUE;
         goto NewRideCard;
      }
*/
      switch( card->PrintFormat )
      {
      case 1:
         if ( ((card->RemVal % 5) == 0) || (PrintFlag == 1) )
         {
            strftime( s, sizeof(s)-1, "   %d %b     %I:%M", c );

            if ( card->RemVal == 0 )
            {
               tprintf( card->PBase+2, card->PrintPos+1, "\\S%02d %s%c",
                          card->RemVal, s, (c->tm_hour<12) ? 2:3 );
            }
            else
            {
               tprintf( card->PBase+2, card->PrintPos, "\\S%02d %s%c",
                        card->RemVal, s, (c->tm_hour<12) ? 2:3 );
               card->PrintPos += 3;
            }
            PrintFlag = 1;
         }

         if ( card->RemVal == 0 )
         {
            strftime( s, sizeof(s)-1, "   %d %b     %I:%M", c );
            tprintf( card->PBase+2, card->PrintPos+1, "\\S%02d %s%c",
                       card->RemVal, s, (c->tm_hour<12) ? 2:3 );
            PrintFlag = 1;
         }
         break;

      case 2:
      case 5:
         if ( card->PrintFormat == 5 )
         {
            tprintf( card->PBase, card->PrintPos, "\\SRIDES   DATE       TIME" );
            tprintf( card->PBase, card->PrintPos+1, Line );
            card->PrintPos += 4;
         }
         strftime( s, sizeof(s)-1, "   %d %b     %I:%M", c );
         tprintf( card->PBase+2, card->PrintPos, "\\S%02d %s%c",
                       card->RemVal, s, (c->tm_hour<12) ? 2:3 );
         card->PrintPos += 2;
         PrintFlag = 1;
         card->PrintFormat = 2;
         break;

      case 3:
         if( (TrimCard.MagCardFlags & EMBDPRNT_1) || 
             (TrimCard.MagCardFlags & EMBDPRNT_2) )
         {
            /*printf( "EMBDPRNT_1 or EMBDPRNT_2\n" );*/
            if( TrimCard.MagCardFlags & EMBDPRNT_1 )
            {
               /*printf( "EMBDPRNT_1\n" );*/
               tprintf( card->PBase-card->PBase+2, card->PrintPos, "%02d",card->RemVal );
               strftime( s, sizeof(s)-1, "%d%b%y",c );
               tprintf( card->PBase, card->PrintPos,"%s", s );
            }
            else
            {
               /*printf( "EMBDPRNT_2\n" );*/
               strftime( s, sizeof(s)-1, "%d%b%y %I:%M",c );
               tprintf( card->PBase, card->PrintPos, "\\S%02d %s%c", 
                           card->RemVal, s, (c->tm_hour<12) ? 2:3 );
               tprintf( card->PBase, card->PrintPos+2, "\\SRTE:%05lu  DIR: %c", 
                           card->IURoute, (DirLet[card->IUDir]) );
            }

            c = gmtime( &card->XferExp );
            s[0] = '\0';
            strftime( s, sizeof(s)-1, "%I:%M", c );
            tprintf( card->PBase+18, card->PrintPos,"%s%c", s, (c->tm_hour<12) ? 2:3 );

            card->PrintPos += 4;

            if( OF5_REVTRP_HLF_XFEREXP )
            {
              now = card->XferExp - (ExpireOffset/2);
              c = gmtime( &now );
              s[0] = '\0';
              strftime( s, sizeof(s)-1, "%I:%M", c );
              tprintf( card->PBase+1, card->PrintPos,
                            "\\SREVERSE: RTE:%05lu  %c  %s%c",
                             card->IURoute, (DirLet[(5-FbxCnf.dir)]), 
                             s, (c->tm_hour<12) ? 2:3 );
            } 
         }
         else
         {
             /* Normal GFi Ride Card format w/Embedded Transfer */
            if( TrimCard.MagCardFlags & EMBDED )
               tprintf( card->PBase, card->PrintPos, "\\SE" );

            strftime( s, sizeof(s)-1, "%d%b %I:%M",c );
            tprintf( card->PBase+1, card->PrintPos, "%02d %s%c", 
                           card->RemVal, s, (c->tm_hour<12) ? 2:3 );
            card->PrintPos += 1;
         }

         card->PrintPos += 3;
         PrintFlag = 1;
         break;
      } 

      time( &now );
      
      if ( card->RemVal == 0 )
      {
         if ( (card->PrintFormat == 1) || (card->PrintFormat == 2) )
         {
            if ( card->PrintFormat == 1 )
               card->PrintPos += 3;

            if( now < card->XferExp )
               tprintf( card->PBase+8, card->PrintPos, "\\STRANSFER ONLY" );
            else
               tprintf( card->PBase+8, card->PrintPos, "\\SEXPIRED TICKET" );
/** AAT - REMOVE **/
            printf( "XferExp %lu\n", card->XferExp );
         }
         else
         {
            if( now < card->XferExp )
               tprintf( card->PBase+1, card->PrintPos+1, "TRANSFER ONLY" );
            else
               tprintf( card->PBase+1, card->PrintPos+1, "EXPIRED TICKET" );
/** AAT - REMOVE **/
            printf( "XferExp %lu\n", card->XferExp );
         }
         PrintFlag = 1;
      }

      if ( card->PrintPos > 63 )
         card->PrintPos = 63;
      break;

   case TRANSFER_MG:
      trips = Dlist.TransferControl[card->TCIndex].NumTrips;
/** AAT - REMOVE **/
      printf( "case TRANSFER_MG:\n" );
/*
      if( card->RemVal == 0 )
         tprintf( card->PBase, 18, "\\D\\VEXPIRED" );
*/
      if ( PUT_EXP && ( ( trips && card->XferTrips >= trips ) ||
        card->XferExp < now ) )
         tprintf( 16, 18, "\\D\\VEXPIRED" );

      PrintFlag = 1;
      break;
   }

   E = 0;

   if( PrintFlag )
   {
      TrimCflags |= CARD_POS_2_PRINT;
      TrimCflags |= CARD_PRINT;
   }

   return E;
}
