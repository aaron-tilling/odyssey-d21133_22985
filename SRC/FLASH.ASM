*-----------------------------------------------------------------------------+
*  Src File:   flash.asm                                                      |
*  Authored:   08/21/97, tgh                                                  |
*  Function:   Flash utilities.                                               |
*  Comments:                                                                  |
*                                                                             |
*  Revision:                                                                  |
*      1.00:   08/27/97, tgh  -  Initial release.                             |
*      1.01:   12/22/98, sjb  -  test vpp on flash on poweron.                |
*      1.03:   06/22/01, sjb  -  modify CalcCS so program greater than 256k   |
*                                can be checked                               |
*              05/28/03, jks  -  stroke watchdog in CalcCS                    |
*                                                                             |
*           Copyright (c) 1993-2003  GFI Genfare All Rights Reserved          |
*-----------------------------------------------------------------------------+

*-----------------------------------------------------------------------------+
*                       Include Files                                         |
*-----------------------------------------------------------------------------+
        include gen.inc
        include fpga.inc
        include regs.inc
        include quart.inc

*-----------------------------------------------------------------------------+
*                       Local Equates                                         |
*-----------------------------------------------------------------------------+

        PCMCIA:         equ     0800000h        ; PCMCIA base address
        PCMCIA_STAT:    equ     070000dh        ; PCMCIA status inputs
        PCMCIA_PGMID:   equ     055aa0001h      ; card holds a program image
        PCMCIA_EXEID:   equ     055aa0002h      ; card holds executable code

        RamLoadAddress: equ     0010d000h       ; relocate code here
        ModuleAddress:  equ     00000000h       ; flash base address

        FLASH_TYPE:     equ     00004000h       ; addrees to check flash type

*
* Am29F800B equates
*
        FLSH_ADDR1:      equ    0aaah       
        FLSH_ADDR2:      equ    0555h  
        FLSH_ADDR3:      equ    0002h  

        FLSH_UNLK1_DAT: equ     00aah
        FLSH_UNLK2_DAT: equ     0055h

        FLSH_RSET_DAT:  equ     00f0h

        FLSH_AUTO3_DAT: equ     0090h
        FLSH_PRGB3_DAT: equ     00a0h
        FLSH_CERA3_DAT: equ     0080h
        FLSH_SERA3_DAT: equ     0080h

        FLSH_UNLK4_DAT: equ     00aah
        FLSH_UNLK5_DAT: equ     0055h

        FLSH_CERA6_DAT: equ     0010h
        FLSH_SERA6_DAT: equ     0030h

        FLSH_MFID_DATA: equ     0001h
        AMD_flash:      equ     02258h          ;
        AMD_sflash:     equ     022abh          ;
        Intel_flash:    equ     0ffffh          ;

*-----------------------------------------------------------------------------+
*                       Local Macros                                          |
*-----------------------------------------------------------------------------+

*       Service watchdog
        _watchdog:      macro
                        move.b  #055h,swsr      ;; service watchdog
                        move.b  #0aah,swsr
                        endm


*-----------------------------------------------------------------------------+
*                       Data Segment (local data)                             |
*-----------------------------------------------------------------------------+
               section  udata,,"data"           uninitialized data

*-----------------------------------------------------------------------------+
*                       External References                                   |
*-----------------------------------------------------------------------------+

*-----------------------------------------------------------------------------+
*                       Code Segment                                          |
*-----------------------------------------------------------------------------+
                section BootCode,,"boot"

*-----------------------------------------------------------------------------+
*  Function:   ChkPCMCIA                                                      |
*  Purpose :   Check the PCMCIA card for a new program or code to run.        |
*  Synopsis:                                                                  |
*  Input   :   None.                                                          |
*  Output  :   None.                                                          |
*  Register:   All preserved.                                                 |
*  Comments:                                                                  |
*-----------------------------------------------------------------------------+
        _fnct   ChkPCMCIA

                movem.l d0-d7/a0-a6,-(a7)       ; save registers

                move.w  FLASH_TYPE,d7
                cmp.w   #AMD_flash,d7
                beq.s   AMD_FLASH
                cmp.w   #AMD_sflash,d7
                bne.s   check_PCMCIA

        AMD_FLASH: 
                move.b  #00111101b,ddre         ; setup for AMD29F800 

        check_PCMCIA:
                move.b  PCMCIA_STAT,d0          ; check CD1 & CD2
                and.b   #09h,d0
                bne     ChkPCMCIA_exit          ; no card just return
                move.b  #00h,led                ; select lower pc memory

                move.l  PCMCIA,d0               ; check ID
                cmp.l   #PCMCIA_PGMID,d0        ; program image
                bne     ChkPCMCIA_chkcode
                jsr     ChkPgmImage
                bra     ChkPCMCIA_exit

        ChkPCMCIA_chkcode:
                cmp.l   #PCMCIA_EXEID,d0        ; program image
                bne     ChkPCMCIA_exit
                jsr     ChkExecutableCode

        ChkPCMCIA_exit:
                movem.l (a7)+,d0-d7/a0-a6       ; restore registers

                rts

*-----------------------------------------------------------------------------+
*                                                                             |
*-----------------------------------------------------------------------------+
        ChkPgmImage:
                move.l  PCMCIA+4,d1             ; d1 == module length
                movea.l #PCMCIA+12,a0           ; a0 -> module data
                jsr     CalcCS
                cmp.l   PCMCIA+8,d0             ; checksums equal ?
                beq.b   ChkPgmImageVerify
                move.b  #06h,led                ; indicate checksum error
                bra     ChkPgmImageWait

        ChkPgmImageVerify:
                move.l  PCMCIA+4,d1             ; d1 == module length
                lsr.l   #2,d1                   ; d1 == module length in longs
                movea.l #PCMCIA+12,a0           ; a0 -> module data
                movea.l #08000h,a1              ; a1 -> destination

                jsr     Verify                  ; verify data in flash
                cmp.w   #0,d0                   ; if it's the same
                bne.b   ChkPgmImageNew
                move.b  #07h,led                ; indicate verified the same
                bra     ChkPgmImageWait         ; just return

        ChkPgmImageNew:
                jsr     RelocateCode            ; move flash code to RAM
                jsr     RamLoadAddress          ; execute flash code in RAM
                cmp.w   #1,d0                   ; check return code
                beq     PgmPowerErr             ; no vpp error      
                cmp.w   #0,d0                   ; check return code
                bne.b   ChkPgmImageErr
                move.b  #0fh,led                ; indicate completion
                bra.b   ChkPgmImageWait

        PgmPowerErr:
                move.b  #0ch,led                ; indicate error
                bra.b   ChkPgmImageWait

        ChkPgmImageErr:
                move.b  #09h,led                ; indicate error

        ChkPgmImageWait:
                _watchdog
                move.b  PCMCIA_STAT,d0          ; check CD1 & CD2
                and.b   #09h,d0
                beq     ChkPgmImageWait         ; card present stay in loop

        ChkPgmImage_exit:
                rts

*-----------------------------------------------------------------------------+
*                                                                             |
*-----------------------------------------------------------------------------+
        ChkExecutableCode:

                move.l  PCMCIA+4,d1             ; d1 == module length
                movea.l #PCMCIA+12,a0           ; a0 -> module data
                jsr     CalcCS
                cmp.l   PCMCIA+8,d0             ; checksums equal ?
                bne     ChkExecutableCode_exit
                jsr     PCMCIA+12               ; start running code in PCMCIA

        ChkExecutableCode_exit:
                rts

*---------------------------------------------------------------------------+
*   Function:   CalcCS                                                      |
*   Synopsis:   int CS = CalcDS( p, n );                                    |
*   Input   :   a0 - points to data                                         |
*   Input   :   d1 - holds number of bytes (should be a multiple of 4 ).    |
*   Output  :   d0 will hold checksum                                       |
*   Comments:   local function to calculate a simple 32-bit checksum.       |
*---------------------------------------------------------------------------+
        CalcCS:
                move.l  #0,d0                   ; initialize accumulator
                cmp.l   #4,d1                   ; check number of bytes
                blo.b   CalcCS_exit
                lsr.l   #2,d1                   ; d1 == number of longs
                bra.b   CalcCS02

        CalcCS01:
                add.l   (a0)+,d0
                _watchdog
        CalcCS02:
		subq.l	#1,d1
		bpl.b	CalcCS01   

        CalcCS_exit:
                rts

*---------------------------------------------------------------------------+
*   Function:   RelocateCode                                                |
*   Synopsis:   void RelocateCode( void );                                  |
*   Input   :   none.                                                       |
*   Output  :   none.                                                       |
*   Comments:   Relocate flash utilities into RAM since we will not be      |
*               able to continue running from flash once we start.          |
*---------------------------------------------------------------------------+
        RelocateCode:
                movea.l #FlashCode,a0           ; a0 -> beginning of code
                movea.l #FlashCodeEnd,a1        ; a1 -> end of code
                movea.l #RamLoadAddress,a2      ; destination address in RAM

        RelocateCode01:
                move.l  (a0)+,(a2)+
                cmpa.l  a0,a1
                bhs     RelocateCode01

                rts

*-----------------------------------------------------------------------------+
*                       Flash programming utilities                           |
*                                                                             |
*       NOTE:  Any subroutines placed in this block must be called with       |
*              the "bsr" mnemonic in place of "jsr".                          |
*                                                                             |
*-----------------------------------------------------------------------------+

        FlashCode:      equ     *               ; address of beginning of code


*-----------------------------------------------------------------------------+
*  Function:   BlockEraseAndProgram()                                         |
*  Purpose :                                                                  |
*  Synopsis:   void  BlockEraseAndProgram( void );                            |
*  Input   :                                                                  |
*  Output  :   int - 0(OK) -1(ERR)                                            |
*  Register:   Flash_Select has device id, 2258h if AMD or 0ffffh if intel    |
*  Comments:                                                                  |
*-----------------------------------------------------------------------------+
        BlockEraseAndProgram:

* first check if AMD or intel flash by checking for amd device id

                movea.l #FLSH_ADDR1,A1          ; setup command address 
                movea.l #FLSH_ADDR2,A2
                movea.l #FLSH_ADDR3,A0          ; address for device id

                move.b  #FLSH_UNLK1_DAT,(A1)    ; unlock flash
                move.b  #FLSH_UNLK2_DAT,(A2)    ; unlock flash
                move.b  #FLSH_AUTO3_DAT,(A1)    ; devices id command
                move.w  (A0),d7                 ; fetch id
                cmp.w   #AMD_flash,d7           ; AMD or intel?
                beq.s   AMD_stuff
                cmp.w   #AMD_sflash,d7          ; AMD or intel?
                bne.s   intel_stuff

           AMD_stuff: 
                move.b  #00111101b,ddre         ; setup for AMD 
                move.b  #FLSH_RSET_DAT,(A1)     ; devices reset
                bra     continue     

           intel_stuff:
                move.b  #01111101b,ddre         ; setup for Intel 
                move.w  #0ffffh,d7              ; save type of flash 
                bsr     PowerOn                 ; turn flash power on
                btst.b  #.bit1,porte            ; vpp on ?
                bne.s   continue                ; branch if yes
                bsr     PowerOff
                move.l  #1,d0                   ; flag error
                bra     ExitBlock

           continue:
                move.b  #011h,led
                movea.l #004000h,a0             ;  8k main block 1
                bsr     Erase

                move.b  #012h,led
                movea.l #008000h,a0             ;  64/96k main block 1
                bsr     Erase

                cmp.w   #AMD_flash,d7           ; AMD or intel?
                beq.s   big_AMD                 ; branch if big AMD 
               
                cmp.w   #AMD_sflash,d7          ; AMD or intel?
                bne.s   intel_flash             ; branch if intel 
                move.b  #010h,d1                ; setup to erase AMD
                move.w  #7,d2
                movea.l #000000h,a0
                bra     erase_again

           big_AMD:
                move.b  #010h,d1                ; setup to erase AMD
                move.w  #15,d2
                movea.l #000000h,a0

           erase_again:
                add.b   #1,d1                
                move.b  d1,led
                add.l   #010000h,a0             ;  64/96k block 1
                bsr     Erase
                sub.w   #1,d2
                bne.s   erase_again
                bra     program_stuff

           intel_flash:
                move.b  #013h,led
                movea.l #020000h,a0             ; 128k main block 2
                bsr     Erase

                move.b  #014h,led
                movea.l #040000h,a0             ; 128k main block 3
                bsr     Erase

                move.b  #015h,led
                movea.l #060000h,a0             ; 128k main block 4
                bsr     Erase

           program_stuff: 
                move.b  #010h,led
                move.l  PCMCIA+4,d1             ; d1 == module length
                lsr.l   #1,d1                   ; d1 == module length in words
                movea.l #PCMCIA+12,a0           ; a0 -> module data
                movea.l #08000h,a1              ; a1 -> destination

                bsr     Write                   ; write data to flash

                cmp.w   #Intel_flash,d7         ; AMD or intel?
                bne     setup_verify            ; branch if AMD 
                bsr     PowerOff                ; turn flash power off
                bra.s   setup_verify
               
           setup_verify:
                move.l  PCMCIA+4,d1             ; d1 == module length
                lsr.l   #2,d1                   ; d1 == module length in longs
                movea.l #PCMCIA+12,a0           ; a0 -> module data
                movea.l #08000h,a1              ; a1 -> destination

                bsr     Verify                  ; verify data in flash

           ExitBlock:
                rts


*-----------------------------------------------------------------------------+
*  Function:   PowerOn()                                                      |
*  Purpose :   Local function to turn on programming voltage.                 |
*  Synopsis:   void  PowerOn( void );                                         |
*  Input   :   None.                                                          |
*  Output  :   None.                                                          |
*  Register:                                                                  |
*  Comments:                                                                  |
*-----------------------------------------------------------------------------+
        PowerOn:

                movem.l d0-d7/a0-a6,-(sp)       ;save registers used

                move.b  #0d2h,0700009h          ; enable "FLSHPGM" via FPGA
                move.b  #010h,led
                bset.b  #6,porte                ; enable "WP#"
                movem.l (sp)+,d0-d7/a0-a6       ; restore registers used

                rts

*-----------------------------------------------------------------------------+
*  Function:   PowerOff()                                                     |
*  Purpose :   Turn of flash programming voltage.                             |
*  Synopsis:   void  PowerOff( void );                                        |
*  Input   :   None.                                                          |
*  Output  :   None.                                                          |
*  Register:                                                                  |
*  Comments:                                                                  |
*-----------------------------------------------------------------------------+
        PowerOff:

                bclr.b  #6,porte                ; set WP#, unlock boot block of flash
                move.b  #00h,led                ; clear Vpp enable bit in fpga
                move.b  #000h,0700009h

                clr.l   d0                      ; flag OK

                rts


*-----------------------------------------------------------------------------+
*  Function:   Erase()                                                        |
*  Purpose :   Erase a block of flash.                                        |
*  Synopsis:   void  Erase( char* );                                          |
*  Input   :   a0 -> address within block to erase                            |
*  Output  :   None.                                                          |
*  Register:                                                                  |
*  Comments:                                                                  |
*-----------------------------------------------------------------------------+
        Erase:
                cmp.w   #Intel_flash,d7 
                beq.s   intel_erase

                move.b  #FLSH_RSET_DAT,(A1)     ; devices reset

                _watchdog                 

                movea.l #FLSH_ADDR1,A1          ; setup command address 
                movea.l #FLSH_ADDR2,A2

                move.b  #FLSH_UNLK1_DAT,(A1)    ; unlock flash
                move.b  #FLSH_UNLK2_DAT,(A2)    ; unlock flash
                move.b  #FLSH_SERA3_DAT,(A1)    ; sector erase command
                move.b  #FLSH_UNLK4_DAT,(A1)    ; unlock flash
                move.b  #FLSH_UNLK5_DAT,(A2)    ; unlock flash
                move.b  #FLSH_SERA6_DAT,(A0)    ; erase chip/sector

        b_e_1:  move.b  (A0),D0
                btst    #7,D0
                bne.b   exit_erase              ; Wait for ready
                btst    #5,D0
                beq.b   b_e_1                   ; time out ?

                move.b  (A0),D0                 ; maybe, check 1 more time
                btst    #7,D0
                bra.s   exit_erase   

        intel_erase:
                move.w  #0020h,(a0)             ; erase command
                move.w  #00d0h,(a0)             ; .

        Erase01:
                _watchdog                       ; wait till ready
                move.w  (a0),d0
                andi.w  #0080h,d0
                beq.s   Erase01

                move.w  #$00ff,(a0)             ; Return to read mode
         exit_erase:
                rts

*-----------------------------------------------------------------------------+
*  Function:   Write()                                                        |
*  Purpose :   Write a block of words into flash.                             |
*  Synopsis:   void  Write( dst, src, n );                                    |
*  Input   :   a0 -> source                                                   |
*              a1 -> destination                                              |
*              d1 -> holds count (number of words)                            |
*  Output  :   None.                                                          |
*  Register:                                                                  |
*  Comments:                                                                  |
*-----------------------------------------------------------------------------+
        Write:
                cmp.w   #Intel_flash,d7 
                beq     intel_write

                movea.l #FLSH_ADDR1,A2
                movea.l #FLSH_ADDR2,A3

                move.b  #FLSH_UNLK1_DAT,(A2)    ; unlock flash
                move.b  #FLSH_UNLK2_DAT,(A3)    ; unlock flash
                move.b  #FLSH_PRGB3_DAT,(A2)    ; program word command
                movea.l #FLASH_TYPE,A2
                move.w  d7,(A2)                 ; save flash type  
        www_1:  _watchdog                       ; wait for status
                move.w  (A2),D0                 ; Get status reg
                cmp.w   d7,D0
                beq.b   www_2                   ; Wait for ready
                btst    #5,d0
                beq.b   www_1

        www_2:  movea.l #FLSH_ADDR1,A2

        ww_1:   move.w  (A0)+,D2
                move.b  #FLSH_UNLK1_DAT,(A2)    ; unlock flash
                move.b  #FLSH_UNLK2_DAT,(A3)    ; unlock flash
                move.b  #FLSH_PRGB3_DAT,(A2)    ; program word command

                move.w  D2,(A1)+                ; Write data from buffer to device
        ww_2:   _watchdog                       ; wait for status
                move.w  (-2,A1),D0              ; Get status reg
                cmp.w   D0,D2
                beq.b   ww_3                    ; Wait for ready
                btst    #5,d0
                beq.b   ww_2
                move.w  (-2,A1),D0              ; Get status reg
                cmp.w   D0,D2
                bne.s   ww_4                    ; timeout

        ww_3:   subq.l  #1,D1                   ; Decrement buffer byte count
                bne.b   ww_1                    ; Loop till no more data

        ww_4:   bra     exite_write

        intel_write: 
                move.w  #0040h,(a1)             ; write command
                move.w  (a0)+,(a1)+             ; write data to flash

        Write01:
                _watchdog                       ; wait for status
                move.w  #0070h,(-2,a1)          ; read status command
                move.w  (-2,a1),d2
                andi.w  #0080h,d2
                beq.b   Write01

                subq.l  #1,d1                   ; update/check word counter
                bpl.b   intel_write

                move.w  #$00ff,(-2,a1)          ; Return to read mode

        exite_write:
                rts

*-----------------------------------------------------------------------------+
*  Function:   Verify()                                                       |
*  Purpose :   Verify a block of long words in flash.                         |
*  Synopsis:   itn   Verify( dst, src, n );                                   |
*  Input   :   a0 -> source                                                   |
*              a1 -> destination                                              |
*              d1 -> holds count (number of long words)                       |
*  Output  :   None.                                                          |
*  Register:                                                                  |
*  Comments:                                                                  |
*-----------------------------------------------------------------------------+
        Verify:
                _watchdog
                cmp.l   (a0)+,(a1)+             ; compare word
                bne.b   VerifyErr
                subq.l  #1,d1                   ; update/check word counter
                bgt.b   Verify                  ; if greater than zero
                move.l  #000000000h,d0          ; flag OK
                bra.b   Verify_exit

        VerifyErr:
                move.l  #0ffffffffh,d0          ; flag error

        Verify_exit:
                rts

*-----------------------------------------------------------------------------+
*                       End Flash programming utilities                       |
*-----------------------------------------------------------------------------+
        FlashCodeEnd:   equ     *               ; end code

