232p.c:           LCDprintf( LCD_C, 1, "%s", TextStr(__OCU_PGM_ERROR) );
232p.c:                LCDprintf( LCD_C, 1, "%s", TextStr(__OCU_UPLD_DONE) );
232p.c:             LCDprintf( 0, 1, "%s %s", TextStr_8(__NO), TextStr(__OCU_UPLD_DONE) );
232p.c:         LCDprintf( 12, 3, " .    " );
232p.c:         LCDprintf( 12, 3, " ..   " );
232p.c:         LCDprintf( 12, 3, " ...  " );
232p.c:         LCDprintf( 12, 3, " .... " );
485p.c:       LCDprintf(     0, 0, "%s",    LCDStr(__LCD_STORED_VAL) );
485p.c:       LCDprintf(     0, 1, "%s",    LCDStr(__LCD_U_HAVE) );
485p.c:       LCDprintf( LCD_R, 1, "%5.2f", (float)val/100.0 );
485p.c:       LCDprintf(     0, 2, "%s",    LCDStr(__LCD_CHARGE) );
485p.c:       LCDprintf( LCD_R, 2, "%5.2f", (float)ded/100.0 );
485p.c:       LCDprintf(     0, 3, "%s",    LCDStr(__LCD_BALANCE) );
485p.c:       LCDprintf( LCD_R, 3, "%5.2f", (float)(val-ded)/100.0 );
485p.c:    LCDprintf(     0, 0, "%s",     LCDStr(__LCD_STORED_VAL) );
485p.c:    LCDprintf(     0, 1, "%s",     LCDStr(__LCD_U_HAVE) );
485p.c:    LCDprintf( LCD_R, 1, "$ 0.00");
485p.c:    LCDprintf(     0, 2, "%s",     LCDStr(__LCD_CHARGE) );
485p.c:    LCDprintf( LCD_R, 2, "$%5.2f", (float)m/100.0 );
485p.c:    LCDprintf(     0, 3, "%s",     TextStr_8(__OWE) );
485p.c:    LCDprintf( LCD_R, 3, "$%5.2f", (float)owed/100.0 );
485p.c:    LCDprintf( LCD_L, 0, "%s",     LCDStr(__LCD_STORED_RIDE) );
485p.c:    LCDprintf( LCD_L, 1, "%s",     LCDStr(__LCD_U_HAVE) );
485p.c:    LCDprintf( LCD_R, 1, "%02d %s", ((CARD*)card)->RemVal,TextStr_8(__RIDE) );
485p.c:    LCDprintf( LCD_L, 2, "%s",     LCDStr(__LCD_FARE) );
485p.c:    LCDprintf( LCD_R, 2, "$%5.2f", (float)m/100.0 );
485p.c:    LCDprintf( LCD_L, 3, "%s",     LCDStr(__LCD_AMT_OWED) );
485p.c:    LCDprintf( LCD_R, 3, "$%5.2f", (float)owed/100.0 );
bvp.c:       LCDprintf( 0, 1, "%s", LCDStr(__PLS_REMOVE) );   /* display */
bvp.c:       LCDprintf( 0, 2, "%s", LCDStr(__YOUR_BILL) );      /* display */
card.c:      LCDprintf(     0, 0, "%s",      LCDStr(__LCD_CRD_INFO_TYPE) );
card.c:      LCDprintf( LCD_R, 0, "%s",      TextStr_8(__UNKNOWN) );
card.c:          LCDprintf(     0, 0, "%s",      LCDStr(__LCD_CRD_INFO_TYPE) );
card.c:          LCDprintf( LCD_R, 0, "%s",      TextStr_8(__PERIOD) );
card.c:          LCDprintf(     0, 2, "%s",      LCDStr(__LCD_CRD_INFO_REM) );
card.c:          LCDprintf( LCD_R, 2, "%s",      TextStr_8(__UNLIMITED) );
card.c:          LCDprintf(     0, 0, "%s",      LCDStr(__LCD_CRD_INFO_TYPE) );
card.c:          LCDprintf( LCD_R, 0, "%s",      TextStr(__CHNG_VAL) );
card.c:          LCDprintf(     0, 2, "%s",      LCDStr(__LCD_CRD_INFO_REM) );
card.c:          LCDprintf( LCD_R, 2, "$%06.2f", (float)card->RemVal/100);
card.c:          LCDprintf(     0, 0, "%s",        LCDStr(__LCD_CRD_INFO_TYPE) );
card.c:          LCDprintf( LCD_R, 0, "%s",        TextStr(__MULT_RIDE) );
card.c:            LCDprintf(     0, 2, "%s",      LCDStr(__LCD_CRD_INFO_REM) );
card.c:            LCDprintf( LCD_R, 2, "%02d %s", card->RemVal, 
card.c:            LCDprintf(     0, 2, "%s",      LCDStr(__LCD_CRD_INFO_REM) );
card.c:            LCDprintf( LCD_R, 2, "%02d %s", card->RemVal, 
card.c:          LCDprintf(     0, 0, "%s",        LCDStr(__LCD_CRD_INFO_TYPE) );
card.c:          LCDprintf( LCD_R, 0, "%s",        TextStr(__MAINT_DVR) );
card.c:          LCDprintf(     0, 0, "%s",        LCDStr(__LCD_CRD_INFO_TYPE) );
card.c:          LCDprintf( LCD_R, 0, "%s",        TextStr_8(__TRANSFER) );
card.c:          LCDprintf(     0, 2, "%s",        LCDStr(__LCD_CRD_INFO_RDS_USD) );
card.c:          LCDprintf( LCD_R, 2, "%8d",       card->XferTrips );
card.c:          LCDprintf( 0, 3, "EXPIRES  :  %02d/%02d/%02d", c->tm_mon+1, c->tm_mday, c->tm_year );
card.c:         LCDprintf(     0, 0, "%s",   LCDStr(__LCD_CRD_INFO_TYPE) );
card.c:         LCDprintf( LCD_R, 0, "%s",   TextStr_8(__INVALID) );
card.c:       LCDprintf(     0, 0, "%s",      LCDStr(__LCD_CRD_INFO_TYPE) );
card.c:       LCDprintf( LCD_R, 0, "%s",      TextStr_8(__PERIOD) );
card.c:         LCDprintf(     0, 0, "%s",      LCDStr(__LCD_CRD_INFO_TYPE) );
card.c:         LCDprintf( LCD_R, 0, "%s",      TextStr_8(__PERIOD) );
card.c:         LCDprintf(     0, 2, "%s",      LCDStr(__LCD_CRD_INFO_REM) );
card.c:         LCDprintf( LCD_R, 2, "%s",      TextStr_8(__UNLIMITED) );
card.c:         LCDprintf(     0, 0, "%s",      LCDStr(__LCD_CRD_INFO_TYPE) );
card.c:         LCDprintf( LCD_R, 0, "%s",      TextStr_8(__DRIVER) );
card.c:         LCDprintf(     0, 2, "%s",      LCDStr(__LCD_CRD_INFO_REM) );
card.c:         LCDprintf( LCD_R, 2, "%s",      TextStr_8(__UNLIMITED) );
card.c:        LCDprintf(     0, 0, "%s",      LCDStr(__LCD_CRD_INFO_TYPE) );
card.c:        LCDprintf( LCD_R, 0, "%s",      TextStr_8(__CREDIT) );
card.c:       LCDprintf(     0, 0, "%s",      LCDStr(__LCD_CRD_INFO_TYPE) );
card.c:       LCDprintf( LCD_R, 0, "%s",      TextStr_8(__UNKNOWN) );
card.c:          LCDprintf(     0, 3, "%s",      LCDStr(__LCD_CRD_INFO_EXP) );
card.c:          LCDprintf( LCD_R, 3, "********" );
card.c:          LCDprintf(     0, 3, "%s",      LCDStr(__LCD_CRD_INFO_EXP) );
card.c:          LCDprintf( LCD_R, 3, "%02d/%02d/%02d", c->tm_mon+1, c->tm_mday, c->tm_year );
card.c:      LCDprintf(     0, 1, "%s", LCDStr(__LCD_CRD_INFO_DISP) );
card.c:      LCDprintf( LCD_R, 1, "%8s", Text );
coin.c:             LCDprintf( LCD_C, 3, "%s", LCDStr(__CHK_COIN_RTN) );
coin.c:             LCDprintf( LCD_C, 3, "%s", LCDStr(__LCD_BLANK) );
coin.c:             LCDprintf( 0, 3, "%s", LCDStr(__LCD_BLANK) );
coin.c:             LCDprintf( 0, 3, "%s", LCDStr(__NO_COIN_ACCPT) );
coin.c:             LCDprintf( 0, 3, "%s", LCDStr(__CHK_COIN_RTN) );        
coin.c:          LCDprintf( 0,3, "%s", LCDStr(__LCD_BLANK) );
ctsdcup.c:         LCDprintf( m->x, m->y, "%s", (char*)m->data );
ctsdcup.c:         LCDprintf( m->x, m->y, "%s", (char*)m->data );
dbgp.c:            LCDprintf( 0, 0, buf );
dbgp.c:            LCDprintf( 0, 1, buf );
dbgp.c:            LCDprintf( 0, 2, buf );
dbgp.c:            LCDprintf( 0, 3, buf );
diag.c:       LCDprintf( 0, 3, " BurnIn -- %02d:%02d:%02d", 
diag.c:   LCDprintf( 0, 0, "%s", LCDStr(__LCD_BLANK) );
diag.c:   LCDprintf( 0, 1, "%s", LCDStr(__LCD_BLANK) );
diag.c:   LCDprintf( 0, 2, "%s", LCDStr(__LCD_BLANK) );
diag.c:     LCDprintf( 0, 3, "%s", LCDStr(__LCD_BLANK) );
diag.c:       LCDprintf( 0, 3, " BurnIn -- %02d:%02d:%02d   ", 
diag.c:       LCDprintf( 0, 3, "%s", LCDStr(__LCD_BLANK) );
diag.c:     LCDprintf( 0, 0, "%s", LCDStr(__LCD_LAMP_TST) );
diag.c:     LCDprintf( 0, 0, "%s", LCDStr(__LCD_MEM_TST) );
diag.c:       LCDprintf( 0, 2, "%s", LCDStr(__NO_EXT_MEM) );
diag.c:       LCDprintf( 0, 1, "%s%4dk", LCDStr(__EXT_MEM_TSTD),((GoodMem+1)*32) );    /* mem a ok */
diag.c:       LCDprintf( 0, 2, "%s%4dk", LCDStr(__NOT_ACCESS),((BadMem)*32) );     /* mem a ok */
diag.c:         LCDprintf( 0, 2, "%s", LCDStr(__BILL_VAL_OOS) );
diag.c:       LCDprintf( 0, 0, "%s", LCDStr(__LCD_BILLVAL_TST) );
diag.c:               LCDprintf( 0, 2, "%s", LCDStr(__BV_EANDE_SENS) );  /* Exit & Optic sensor faults */
diag.c:               LCDprintf( 0, 2, "%s", LCDStr(__BV_EXIT_SENS) );  /* Exit sensor fault */
diag.c:               LCDprintf( 0, 2, "%s", LCDStr(__BV_ENTRY_SENS) );  /* Entry sensor fault */
diag.c:               LCDprintf( 0, 2, "%s", LCDStr(__BV_UND_FLT) );  /* Undefined sensor fault */
diag.c:               LCDprintf( 0, 2, "%s%4ld ", LCDStr(__BV_RST_CNT),BvpResetCnt );
diag.c:               LCDprintf( 0, 2, "%s", LCDStr(__LCD_BLANK) );
diag.c:               LCDprintf( 0, 2, "%s", LCDStr(__BV_NO_COMM) );  /* No communications */
diag.c:               LCDprintf( 0, 2, "%s", LCDStr(__BV_ENT_EXIT_OPT) );  /* Entry, Exit & Optic sensor faults */
diag.c:               LCDprintf( 0, 2, "%s", LCDStr(__BV_EXIT_OPT_SENS) );  /* Exit & Optic sensor faults */
diag.c:               LCDprintf( 0, 2, "%s", LCDStr(__BV_ENT_OPT_SENS) );  /* Entry & Optic sensor faults */
diag.c:               LCDprintf( 0, 2, "%s", LCDStr(__BV_OPT_SENS) );  /* Optic sensor fault */
diag.c:               LCDprintf( 0, 2, "%s", LCDStr(__BV_ENT_EXIT_MAG) );  /* Entry, Exit & Magnetic sensor faults */
diag.c:               LCDprintf( 0, 2, "%s", LCDStr(__BV_EXIT_MAG_SENS) );  /* Exit & Magnetic sensor faults */
diag.c:               LCDprintf( 0, 2, "%s", LCDStr(__BV_ENT_MAG_SENS) );  /* Entry & Magnetic sensor faults */
diag.c:               LCDprintf( 0, 2, "%s", LCDStr(__BV_MAG_SENS) );  /* Magnetic sensor fault */
diag.c:               LCDprintf( 0, 2, "%s", LCDStr(__BV_ENT_EXIT_MOT) );  /* Entry, Exit & Motor sensor faults */
diag.c:               LCDprintf( 0, 2, "%s", LCDStr(__BV_EXIT_SENS_MOT) );  /* Exit & Motor sensor faults */
diag.c:               LCDprintf( 0, 2, "%s", LCDStr(__BV_ENT_SENS_MOT) );  /* Entry & Motor sensor faults */
diag.c:               LCDprintf( 0, 2, "%s", LCDStr(__BV_MOTOR) );  /* Motor fault */
diag.c:               LCDprintf( 0, 2, "%s", LCDStr(__BV_ENT_EXIT_SENS) );  /* Entry & Exit sensor faults */
diag.c:               LCDprintf( 0, 2, "     Exit Sensor    " );  /* Exit sensor fault */
diag.c:               LCDprintf( 0, 2, "  Entry & Exit Sens " );  /* Entry & Exit sensor faults */
diag.c:               LCDprintf( 0, 2, "     Exit Sensor    " );  /* Exit sensor fault */
diag.c:               LCDprintf( 0, 2, "    Entry Sensor    " );  /* Entry sensor fault */
diag.c:               LCDprintf( 0, 2, "   Undefined fault  " );  /* Undefined sensor fault */
diag.c:         LCDprintf( 0, 2, "         OK         " );
diag.c:     LCDprintf( 0, 0, "OnTime %02lds  Ref %4ld",
diag.c:     LCDprintf( 0, 1, "Loop Count %02d     ", LoopCnt+1 );
diag.c:          LCDprintf( 0, 0, "OnTime %02lds  Ref %4ld",
diag.c:          LCDprintf( 0, 1, "Loop Count %02d     ", LoopCnt+1 );
diag.c:          LCDprintf( 0, 0, " BILL TRANSPORT TEST" );
diag.c:          LCDprintf( 0, 1, "OnTime %02lds  Ref %4ld",
diag.c:       LCDprintf( 0, 2, "Last Cyc %3ld (%3ld%%)", 
diag.c:           LCDprintf( 7, 1, "FAILED" );
diag.c:             LCDprintf( 0, 2, "Chopper Error Detect" );
diag.c:           LCDprintf( 0, 3, "Below 95%% %3d (T%3d%)",
diag.c:         LCDprintf( 0, 3, "                     " );
diag.c:         LCDprintf( 7, 1, "PASSED" ); 
diag.c:       LCDprintf( 0, 2, "Chopper Error Detect" );
diag.c:     LCDprintf( 0, 0, "     SOUND TEST     " );
diag.c:         LCDprintf( 0, 2, "   Trim in Manual   " );
diag.c:         LCDprintf( 0, 2, "   Trim in Bypass   " );
diag.c:         LCDprintf( 0, 2, "Clear path Count %3d", BurnInDiag.clr_jam );
diag.c:         LCDprintf( 0, 2, "    Trim Offline    " );
diag.c:          LCDprintf( 0, 0, "     TRIM TEST      " );
diag.c:           LCDprintf( 0, 2, "  Check TRiM Bezel  " );
diag.c:           LCDprintf( 0, 2, " TRiM Card at Exit  " );
diag.c:         LCDprintf( 0, 0, "     TRIM TEST      " );
diag.c:           LCDprintf( 0, 2, "    Trim Offline    " );
diag.c:           LCDprintf( 0, 2, "    Trim no Stock   " );
diag.c:           LCDprintf( 0, 2, "  Check TRiM Bezel  " );
diag.c:           LCDprintf( 0, 2, " TRiM Card at Exit  " );
diag.c:           LCDprintf( 0, 2, "    Trim Missfeed   " );
diag.c:       LCDprintf( 0, 0, "Mem Good      %4dk ", (((BurnInDiag.MemGood)*32)+256) );
diag.c:       LCDprintf( 0, 0, "Mem No Acces  %4dk ", ((24-BurnInDiag.MemGood)*32) );
diag.c:       LCDprintf( 0, 1, "Validator      NO BV" );  /* Entry & Exit sensor faults */
diag.c:         LCDprintf( 0, 1, "Validator        OOS" );  /* Entry & Exit sensor faults */
diag.c:                  LCDprintf( 0, 1, "Validator  ENT & EXT" );  /* Entry & Exit sensor faults */
diag.c:                  LCDprintf( 0, 1, "Validator  EXIT SENS" );  /* Exit sensor fault */
diag.c:                  LCDprintf( 0, 1, "Validator  ENT SENSR" );  /* Entry sensor fault */
diag.c:                  LCDprintf( 0, 1, "Validator  UNDEFINED" );  /* Entry sensor fault */
diag.c:                  LCDprintf( 0, 1, "Validator         OK" );  /* No faults */
diag.c:                LCDprintf( 0, 1, "Validator    NO COMM" );  /* No communications */
diag.c:                   LCDprintf( 0, 1, "Validator  EN EX OPT" );  /* Entry, Exit & Optic sensor faults */
diag.c:                  LCDprintf( 0, 1, "Validator  EXT & OPT" );  /* Exit & Optic sensor faults */
diag.c:                  LCDprintf( 0, 1, "Validator  ENT & OPT" );  /* Entry & Optic sensor faults */
diag.c:                  LCDprintf( 0, 1, "Validator  OPT SENSR" );  /* Optic sensor fault */
diag.c:                  LCDprintf( 0, 1, "Validator  EN EX MAG" );  /* Entry, Exit & Magnetic sensor faults */
diag.c:                  LCDprintf( 0, 1, "Validator  EXT & MAG" );  /* Exit & Magnetic sensor faults */
diag.c:                  LCDprintf( 0, 1, "Validator  ENT & MAG" );  /* Entry & Magnetic sensor faults */
diag.c:                  LCDprintf( 0, 1, "Validator  MAG SENSR" );  /* Magnetic sensor fault */
diag.c:                  LCDprintf( 0, 1, "Validator  EN EX MOT" );  /* Entry, Exit & Motor sensor faults */
diag.c:                  LCDprintf( 0, 1, "Validator  EXT & MOT" );  /* Exit & Motor sensor faults */
diag.c:                  LCDprintf( 0, 1, "Validator  ENT & MOT" );  /* Entry & Motor sensor faults */
diag.c:                  LCDprintf( 0, 1, "Validator      MOTOR" );  /* Motor sensor fault */
diag.c:                  LCDprintf( 0, 1, "Validator  ENT & EXT" );  /* Entry & Exit sensor faults */
diag.c:                  LCDprintf( 0, 1, "Validator  EXIT SENS" );  /* Exit sensor fault */
diag.c:                  LCDprintf( 0, 1, "Validator  ENT & EXT" );  /* Entry & Exit sensor faults */
diag.c:                  LCDprintf( 0, 1, "Validator  EXIT SENS" );  /* Exit sensor fault */
diag.c:                  LCDprintf( 0, 1, "Validator  ENT SENSR" );  /* Entry sensor fault */
diag.c:                  LCDprintf( 0, 1, "Validator  UNDEFINED" );  /* Undefined sensor fault */
diag.c:         LCDprintf( 0, 1, "Validator         OK" );
diag.c:       LCDprintf( 0, 2, "Transport   NO TRANS" );
diag.c:           LCDprintf( 0, 2, "Transport   3 in ROW" );
diag.c:           LCDprintf( 0, 2, "Transport   LOW > 10" );
diag.c:           LCDprintf( 0, 2, "Transport    0 COUNT" );
diag.c:             LCDprintf( 0, 2, "Transport   ENDED OK" );
diag.c:             LCDprintf( 0, 2, "Transport         OK" );
diag.c:       LCDprintf( 0, 3, "TRiM         NO TRIM" );
diag.c:          LCDprintf( 0, 3, "TRiM        OFF LINE" );
diag.c:            LCDprintf( 0, 3, "TRiM             OOS" );
diag.c:            LCDprintf( 0, 3, "TRiM        NO STOCK" );
diag.c:            LCDprintf( 0, 3, "TRiM           BEZEL" );
diag.c:            LCDprintf( 0, 3, "TRiM            EXIT" );
diag.c:            LCDprintf( 0, 3, "TRiM         MISFEED" );
diag.c:          LCDprintf( 0, 3, "TRiM    EXCESS ISSUE" );
diag.c:          LCDprintf( 0, 3, "TRiM      W/C STARTS" );
diag.c:          LCDprintf( 0, 3, "TRiM      R/V ERRORS" );
diag.c:              LCDprintf( 0, 3, "TRiM        ENDED OK" );
diag.c:                LCDprintf( 0, 3, "TRiM     MODE-MANUAL " );
diag.c:                LCDprintf( 0, 3, "TRiM     MODE-BYPASS " );
diag.c:                LCDprintf( 0, 3, "TRiM     MODE-NORMAL " );
diag.c:         LCDprintf( 0, 3, " BurnIn -- %02d:%02d:%02d", 
lcdc.c: |           04/08/05, aat - in LCDprintf() if language setting from Data      |
lcdc.c:    LCDprintf( LCD_C, 1, TextStr_8(__PLEASE) );
lcdc.c:    LCDprintf( LCD_C, 2, TextStr(__TAKE_RECEIPT) );
lcdc.c:          LCDprintf( LCD_C, 3, LCDStr(__LCD_PRNT_RECEIPT) );
lcdc.c:          LCDprintf( LCD_C, 3, LCDStr(__LCD_NO_RECEIPT_WANTED) );
lcdc.c:    LCDprintf( LCD_C, 1, LCDStr(__LCD_WANT_RECEIPT) );
lcdc.c:    LCDprintf( LCD_L, 2, LCDStr(__LCD_NO__YES) ); 
lcdc.c:       LCDprintf( center_x( buffer ), 0, buffer );
lcdc.c:    LCDprintf( LCD_L, 2, LCDStr(__LCD_PRB_ENB) );
lcdc.c:       LCDprintf( center_x( buffer ), 1, buffer );
lcdc.c:    LCDprintf( LCD_C, 1, LCDStr(__LCD_CHNG_IS_ENC) );
lcdc.c:    LCDprintf( LCD_C, 2, TextStr_8(__ON_CRD) );
lcdc.c:    LCDprintf( LCD_C, 1, LCDStr(__LCD_CHNG_ENC_ON) );
lcdc.c:    LCDprintf( LCD_L, 2, TextStr(__CRD_IS) );
lcdc.c:    LCDprintf( LCD_R, 2, " $%5.2f", (float)cash/100.0 );
lcdc.c:    LCDprintf( LCD_C, 1, LCDStr(__LCD_VAL_ENC_ON) );
lcdc.c:    LCDprintf( LCD_L, 2, TextStr(__CRD_IS) );
lcdc.c:    LCDprintf( LCD_R, 2, " $%5.2f", (float)cash/100.0 );
lcdc.c:        LCDprintf( LCD_C, 1, LCDStr(__PLS_PRESENT_CRD) );
lcdc.c:        LCDprintf( LCD_C, 2, "%08ld", SmartCD_Seq );
lcdc.c:        LCDprintf( LCD_C, 3, LCDStr(__LCD_SAT_FARE) );
lcdc.c:        LCDprintf( LCD_L, 1, LCDStr(__LCD_AMT_OWED) );
lcdc.c:        LCDprintf( LCD_R, 1, "$%5.2f", (float)SC_Card.LuVal/100.0 ); /* display */ 
lcdc.c:        LCDprintf( LCD_L, 2, LCDStr(__LCD_INSERTED) );
lcdc.c:        LCDprintf( LCD_R, 2, "$%5.2f", (float)Driver_Rev/100.0 );    /* display */ 
lcdc.c:      LCDprintf( LCD_L, 2, LCDStr(__LCD_INSERTED) );
lcdc.c:      LCDprintf( LCD_R, 2, "$%5.2f", (float)Driver_Rev/100.0 );    /* display */ 
lcdc.c:       LCDprintf( LCD_L, 0, LCDStr(__LCD_CANCEL__ACCEPT) ); /* display     */ 
lcdc.c:             LCDprintf( LCD_L, 1, LCDStr(__LCD_CRD_VAL) );
lcdc.c:             LCDprintf( LCD_R, 1, "$%5.2f", (float)SC_Card.RemVal/100.0 ); /* display     */ 
lcdc.c:             LCDprintf( LCD_L, 3, LCDStr(__LCD_TOT_VAL) );
lcdc.c:             LCDprintf( LCD_R, 3, "$%5.2f", (float)(Driver_Rev+SC_Card.RemVal)/100.0 ); /* display     */ 
lcdc.c:          LCDprintf( LCD_L, 1, LCDStr(__LCD_CRD_VAL) );
lcdc.c:          LCDprintf( LCD_R, 1, "$%5.2f", (float)TrimCard.RemVal/100.0 ); /* display     */ 
lcdc.c:          LCDprintf( LCD_L, 3, LCDStr(__LCD_TOT_VAL) );
lcdc.c:          LCDprintf( LCD_R, 3, "$%5.2f", (float)(Driver_Rev+TrimCard.RemVal)/100.0 ); /* display     */ 
lcdc.c:              LCDprintf( LCD_L, 1, "%-8s %d  %s",
lcdc.c:               LCDprintf( 0, 1, "%-8s %d    $%5.2f", 
lcdc.c:               LCDprintf( LCD_L, 0, LCDStr(__LCD_BIL_CNT) );
lcdc.c:               LCDprintf( LCD_R, 0, "%4d", CashboxBill);
lcdc.c:            LCDprintf( 0, 1, "%8s      $%5.2f", 
lcdc.c:          LCDprintf( LCD_L, 0, LCDStr(__LCD_STORED_RIDE) );
lcdc.c:          LCDprintf( LCD_R, 0, buff );
lcdc.c: /*         LCDprintf( 0, 0, Buff ); */
lcdc.c:          LCDprintf( LCD_L, 1, LCDStr(__LCD_U_HAVE) );
lcdc.c:          LCDprintf( LCD_R, 1, "%02d", i );
lcdc.c:          LCDprintf( LCD_L, 2, LCDStr(__LCD_USING) );
lcdc.c:          LCDprintf( LCD_R, 2, "1" );
lcdc.c:          LCDprintf( LCD_L, 3, LCDStr(__LCD_RIDES_REM) );
lcdc.c:          LCDprintf( LCD_R, 3, "%02d", i );
lcdc.c:        LCDprintf( center_x(buff), 1, buff );
lcdc.c:    LCDprintf( LCD_C, 1, TextStr(__BUTTON_USED) );
lcdc.c:    LCDprintf( LCD_C, 2, TextStr_8(__FOR) );
lcdc.c:    LCDprintf( LCD_C, 3, TextStr(__PASNGR_INPUT) );
lcdc.c:                 LCDprintf( LCD_L, 2, LCDStr(__LCD_LT_BUTTON) );
lcdc.c:                 LCDprintf( LCD_L, 1, LCDStr(__PLS_PRESENT_CRD) );
lcdc.c:                 LCDprintf( LCD_L, 2, "      %8ld          ", SmartCD_Seq );
lcdc.c:                 LCDprintf( LCD_L, 3, LCDStr(__LCD_FOR_RECH) );
lcdc.c:                 LCDprintf( LCD_L, 1, LCDStr(__PLS_PRESENT_CRD) );
lcdc.c:                 LCDprintf( LCD_L, 3, LCDStr(__LCD_FOR_RECH) );
lcdc.c:                 LCDprintf( LCD_L, 2, LCDStr(__LCD_RT_BUTTON) );
lcdc.c:                 LCDprintf( LCD_C, 2, TextStr(__TOP_CLOSED) );
lcdc.c:                 LCDprintf( LCD_C, 2, TextStr(__TOP_OPEN) );
lcdc.c:     LCDprintf( LCD_C, 1, LCDStr(__LCD_COIN_TST) );
lcdc.c:         LCDprintf( center_x(s5), 0, s5 );
lcdc.c:         LCDprintf( LCD_C, 1, LCDStr(__LCD_CTRL_UNIT_OFF) );
lcdc.c:           LCDprintf( LCD_L, 3, TextStr(__SCANNING) );
lcdc.c:           LCDprintf( LCD_C, 2, LCDStr(__LCD_BLANK) );
lcdc.c:         LCDprintf( center_x(s5), 0, s5 );
lcdc.c:         LCDprintf( LCD_C, 1, LCDStr(__LCD_CTRL_UNIT_OFF) );
lcdc.c:         LCDprintf( LCD_L, 3, TextStr(__SCANNING) );
lcdc.c:            LCDprintf( center_x(LCDHeader), 1, LCDHeader );
lcdc.c:            LCDprintf( center_x(LCDHeader2), 2, LCDHeader2 );
lcdc.c:     LCDprintf( LCD_C, 1, LCDStr(__LCD_COIN_TST) );
lcdc.c:       LCDprintf( LCD_C, 2, TextStr(__OUT_OF_SERVICE) );
lcdc.c:           LCDprintf( LCD_C, 1, TextStr(__OUT_OF_SERVICE) );
lcdc.c:           LCDprintf( LCD_C, 1, "%s %s", TextStr_8(__PRESENT),TextStr_8(__SMARTCRD) );
lcdc.c:         LCDprintf( LCD_C, 1, "%s %s", TextStr_8(__INS),TextStr_8(__MAGS) );
lcdc.c:           LCDprintf( LCD_C, 2, TextStr_8(__OR) );
lcdc.c:           LCDprintf( LCD_C, 3, "%s %s", TextStr_8(__PRESENT),TextStr_8(__SMARTCRD) );
lcdc.c:           LCDprintf( 0, 0, (Tst_Sflag(S_CASHBOX)) ? 
lcdc.c:           LCDprintf( 0, 0, (Tst_Sflag(S_CASHBOX)) ?           
lcdc.c:       LCDprintf( LCD_C, 2, TextStr(__OUT_OF_SERVICE) );
lcdc.c:           LCDprintf( LCD_C, 0, TextStr_8(__LIDOPEN) );
lcdc.c:           LCDprintf( LCD_C, 1, "%s  %s", TextStr(__NO_CASHBOX),TextStr_8(__UNLOCKED) );
lcdc.c:           LCDprintf( LCD_C, 1, "%s  %s", TextStr(__NO_CASHBOX),TextStr_8(__LIDOPEN) );
lcdc.c:            LCDprintf( LCD_C, 1, "%s    %s", TextStr_8(__UNLOCKED),TextStr_8(__LIDOPEN) );
lcdc.c:           LCDprintf( LCD_C, 1, TextStr_8(__LIDOPEN) );
lcdc.c:           LCDprintf( LCD_C, 1, "%s  %s", TextStr(__NO_CASHBOX),TextStr_8(__UNLOCKED) );
lcdc.c:           LCDprintf( LCD_C, 1, TextStr(__NO_CASHBOX) );
lcdc.c:           LCDprintf( LCD_C, 1, TextStr_8(__UNLOCKED) );
lcdc.c:         LCDprintf( center_x(s1), 0, s1 );
lcdc.c:          LCDprintf( LCD_C, 1, LCDStr(__LCD_CTRL_UNIT_OFF) );
lcdc.c:          LCDprintf( LCD_L, 2, LCDStr(__LCD_BLANK) );
lcdc.c:          LCDprintf( 4, 3, TextStr(__SCANNING) );
lcdc.c:           LCDprintf( center_x(LCDHeader), 1, LCDHeader );
lcdc.c:           LCDprintf( center_x(LCDHeader2), 2, LCDHeader2 );
lcdc.c:             LCDprintf( LCD_C, 1, TextStr_8(__INS) );
lcdc.c:             LCDprintf( LCD_C, 1, "%s %s",TextStr_8(__INS),TextStr_8(__COINS) );
lcdc.c:             LCDprintf( LCD_C, 1, "%s %s",TextStr_8(__INS),TextStr_8(__BILLS) );
lcdc.c:             LCDprintf( LCD_C, 0, TextStr_8(__INS) );
lcdc.c:             LCDprintf( LCD_C, 1, "%s, %s",TextStr_8(__COINS),TextStr_8(__BILLS) );
lcdc.c:               LCDprintf( LCD_C, 2, "%s %s",TextStr_8(__OR),TextStr_8(__MAGS) );
lcdc.c:               LCDprintf( LCD_C, 2, TextStr_8(__MAGS) );
lcdc.c:               LCDprintf( LCD_C, 2, TextStr_8(__MAGS) );
lcdc.c:               LCDprintf( LCD_C, 3, "%s %s",TextStr_8(__OR),TextStr_8(__XFERS) );
lcdc.c:               LCDprintf( LCD_C, 2, "%s, %s",TextStr_8(__MAGS),TextStr_8(__XFERS) );
lcdc.c:             LCDprintf( LCD_C, 3, "%s %s",TextStr_8(__OR),TextStr_8(__SMARTCRD) );
lcdc.c:             LCDprintf( 0, 0, (Tst_Sflag(S_CASHBOX)) ? 
lcdc.c:             LCDprintf( 0, 0, (Tst_Sflag(S_CASHBOX)) ?           
lcdc.c:         LCDprintf( LCD_C, 2, TextStr(__OUT_OF_SERVICE) );
lcdc.c:             LCDprintf( LCD_C, 0, TextStr_8(__LIDOPEN) );
lcdc.c:             LCDprintf( LCD_C, 1, "%s  %s", TextStr(__NO_CASHBOX),TextStr_8(__UNLOCKED) );
lcdc.c:             LCDprintf( LCD_C, 1, "%s  %s", TextStr(__NO_CASHBOX),TextStr_8(__LIDOPEN) );
lcdc.c:              LCDprintf( LCD_C, 1, "%s    %s",TextStr_8(__UNLOCKED),TextStr_8(__LIDOPEN) );
lcdc.c:             LCDprintf( LCD_C, 1, TextStr_8(__LIDOPEN) );
lcdc.c:             LCDprintf( LCD_C, 1, "%s  %s", TextStr(__NO_CASHBOX),TextStr_8(__UNLOCKED) );
lcdc.c:             LCDprintf( LCD_C, 1, TextStr(__NO_CASHBOX) );
lcdc.c:             LCDprintf( LCD_C, 1, TextStr_8(__UNLOCKED) );
lcdc.c:         LCDprintf( center_x(s1), 0, s1 );
lcdc.c:         LCDprintf( LCD_C, 1, TextStr(__OUT_OF_SERVICE) ); 
lcdc.c:         LCDprintf( LCD_L, 2, LCDStr(__LCD_BLANK) );
lcdc.c:           LCDprintf( LCD_L, 3, TextStr(__SCANNING) );
lcdc.c:           LCDprintf( LCD_L, 3, LCDStr(__LCD_BLANK) );
lcdc.c:   LCDprintf( LCD_C, 3, "%s %s", Copyright,TextStr(__GFI_GENFARE) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_CRD_CREATED) );
lcdc.c:           LCDprintf( LCD_C, 0, LCDStr(__LCD_SOFTWARE_VERS) );
lcdc.c:           LCDprintf( LCD_L, 1, "FBX v%-8ld",(long*)Mlist.Config.FareBoxVer );
lcdc.c:               LCDprintf( 9, 1, "*TRiM v%-8ld", (long*)Ml.trim_v );
lcdc.c:               LCDprintf( LCD_R, 1, TextStr_8(__NO_TRIM) );
lcdc.c:             LCDprintf( 10, 1, "TRiM v%-8ld", (long*)Ml.trim_v );
lcdc.c:             LCDprintf( LCD_R, 1, "           ", (long*)Ml.trim_v );
lcdc.c:           LCDprintf( LCD_L, 2, "OCU v%-8d", OCU_ver );
lcdc.c:           LCDprintf( LCD_R, 2, "DS v%-5d", Prb_Req_Parms.ver );
lcdc.c:               LCDprintf( 0, 3, "SC  v%-d.%d", SSC_base, SSC_ver  );
lcdc.c:               LCDprintf( 0, 3, "SC  v%-d.%d", SSC_base, SSC_ver  );
lcdc.c:           LCDprintf( LCD_R, 3, TextStr_8(__NEXT_ARW) );
lcdc.c:           LCDprintf( LCD_C, 0, LCDStr(__LCD_LANG_SPAN) );
lcdc.c:           LCDprintf( LCD_C, 0, LCDStr(__LCD_LANG_FREN) );
lcdc.c:           LCDprintf( LCD_C, 0, LCDStr(__LCD_LANG_4) );
lcdc.c:           LCDprintf( LCD_C, 0, LCDStr(__LCD_LANG_ENG) );
lcdc.c:             LCDprintf( LCD_C, 0, LCDStr(__LCD_CHNG_2_SPAN) );
lcdc.c:             LCDprintf( LCD_C, 0, LCDStr(__LCD_CHNG_2_FREN) );
lcdc.c:             LCDprintf( LCD_C, 0, LCDStr(__LCD_CHNG_2_4) );
lcdc.c:             LCDprintf( LCD_C, 0, LCDStr(__LCD_CHNG_2_ENG) );
lcdc.c:         LCDprintf( LCD_C, 0, LCDStr(__LCD_CNTRY_CAN) );
lcdc.c:         LCDprintf( LCD_C, 0, LCDStr(__LCD_CNTRY_US) );
lcdc.c:         LCDprintf( LCD_C, 0, LCDStr(__LCD_CFG_SAL) );
lcdc.c:         LCDprintf( LCD_C, 0, LCDStr(__LCD_CFG_FBX) );
lcdc.c:       LCDprintf( LCD_C, 0, LCDStr(__LCD_CTRL_UNIT) );
lcdc.c:           LCDprintf( LCD_C, 1, LCDStr(__LCD_GFI_W_MENU) );
lcdc.c:           LCDprintf( LCD_C, 1, LCDStr(__LCD_GFI_NO_MENU) );
lcdc.c:           LCDprintf( LCD_C, 1, LCDStr(__LCD_CTS_DCU) );
lcdc.c:             LCDprintf( LCD_C, 1, LCDStr(__LCD_CTS_DCU) );
lcdc.c:               LCDprintf( LCD_C, 1, LCDStr(__LCD_GFI_NO_MENU) );
lcdc.c:               LCDprintf( LCD_C, 1, LCDStr(__LCD_GFI_W_MENU) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_CHNG_9600) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_CHNG_19200) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_CHNG_38400) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_CHNG_57600) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_CHNG_115200) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_CHNG_230400) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_IR_9600) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_IR_19200) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_IR_38400) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_IR_57600) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_IR_115200) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_IR_230400) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_CHNG_9600) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_CHNG_19200) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_CHNG_38400) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_CHNG_57600) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_OCU_9600) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_OCU_9600) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_OCU_9600) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_OCU_9600) );
lcdc.c:         LCDprintf( LCD_C, 0, LCDStr(__LCD_CBID_ENB) );
lcdc.c:         LCDprintf( LCD_L, 3, "ID:%8ld", Ml.cbx_n );
lcdc.c:         LCDprintf( LCD_R, 3, TextStr_8(__NEXT_ARW) );
lcdc.c:         LCDprintf( LCD_C, 0, LCDStr(__LCD_CBID_DIS) );
lcdc.c:         LCDprintf( LCD_R, 0, TextStr_8(__OFFLINE) );
lcdc.c:         LCDprintf( LCD_R, 0, TextStr_8(__ONLINE) );
lcdc.c:           LCDprintf( LCD_L, 0, TextStr(__SONY_SC) );
lcdc.c:           LCDprintf( LCD_L, 0, TextStr(__OTI_SC) );
lcdc.c:           LCDprintf( LCD_L, 0, TextStr(__ASCOM_SC) );
lcdc.c:           LCDprintf( LCD_C, 0, LCDStr(__LCD_SC_DIS) );
lcdc.c:             LCDprintf( LCD_C, 0, LCDStr(__LCD_CHG_2_SONY) );
lcdc.c:             LCDprintf( LCD_C, 0, LCDStr(__LCD_CHG_2_OTI) );
lcdc.c:             LCDprintf( LCD_C, 0, LCDStr(__LCD_CHG_2_ASCOM) );
lcdc.c:             LCDprintf( LCD_C, 0, LCDStr(__LCD_DIS_SC) );
lcdc.c:           LCDprintf( LCD_C, 0, LCDStr(__LCD_BV_JAMMED) );
lcdc.c:           LCDprintf( LCD_C, 0, LCDStr(__LCD_BV_OOS) );
lcdc.c:         LCDprintf( LCD_C, 2, LCDStr(__LCD_ATPT_RST) );
lcdc.c:         LCDprintf( LCD_C, 0, LCDStr(__LCD_BV_OK) );
lcdc.c:           LCDprintf( LCD_C, 2, LCDStr(__LCD_CHK_BV_CONF) );
lcdc.c:                 LCDprintf(  LCD_C, 0, LCDStr(__LCD_SLOWDOWN_TRIM) );
lcdc.c:                 LCDprintf(  LCD_C, 0, LCDStr(__LCD_SPEEDUP_TRIM) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_CLR_PATH) );
lcdc.c:                 LCDprintf( LCD_C, 0, LCDStr(__LCD_MAN_TRIM) );
lcdc.c:                   LCDprintf(  LCD_C, 0, LCDStr(__LCD_TURN_TBYPASS_OFF) );
lcdc.c:                   LCDprintf(  LCD_C, 0, LCDStr(__LCD_TURN_TBYPASS_ON) );
lcdc.c:                 LCDprintf( LCD_C, 0, LCDStr(__LCD_TSTAT_LED_OFF) );
lcdc.c:                 LCDprintf( LCD_C, 0, LCDStr(__LCD_TSTAT_LED_ON) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_ISS_TST) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_ISS_DIFF) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_ISS_CUMU) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_READ_TST) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_RTN_TST) );
lcdc.c:             LCDprintf( LCD_C, 2, LCDStr(__LCD_INS_CRD) );
lcdc.c:             LCDprintf( LCD_C, 2, LCDStr(__LCD_CHG_NOT_ALLOW) );
lcdc.c:             LCDprintf( LCD_C, 0, LCDStr(__LCD_CFG_FOR) );
lcdc.c:             LCDprintf( LCD_C, 2, TextStr_8(__NO_TRIM) );
lcdc.c:               LCDprintf( LCD_L, 0, TextStr(__TRIM_OFFLINE) );
lcdc.c:               LCDprintf( LCD_L, 0, TextStr(__TRIM_ONLINE) );
lcdc.c:               LCDprintf( LCD_R, 0, TextStr_8(__FAST) );
lcdc.c:               LCDprintf( LCD_R, 0, TextStr_8(__SLOW) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_BILLVAL_TST) );
lcdc.c:                 LCDprintf( LCD_C, 2, LCDStr(__LCD_CFG_NO_VAL) );
lcdc.c:                 LCDprintf( LCD_C, 2, LCDStr(__LCD_VAL_OOS) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_BT_TST) );
lcdc.c:                 LCDprintf( LCD_C, 2, LCDStr(__LCD_CFG_NO_BILL) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_SOUND_TST) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_LAMP_TST) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_MEM_TST) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_TRIM_TST) );
lcdc.c:                 LCDprintf( LCD_C, 2, LCDStr(__LCD_CFG_NO_TRIM) );
lcdc.c:                 LCDprintf( LCD_C, 2, LCDStr(__LCD_TRM_OL) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_DISP_TST) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_BV_BT_TST) );
lcdc.c:               LCDprintf( LCD_C, 0, __LCD_TRIM_BT_TST );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_TST_ALL) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_CPU_TST) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_ODY_SELFTEST) );
lcdc.c:           LCDprintf( LCD_C, 0, LCDStr(__LCD_ODY_SELFTEST) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_PRGM_CRD) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_FARE_CRD) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_CONF_CRD) );
lcdc.c:                 LCDprintf( LCD_C, 0, LCDStr(__LCD_FRND_CRD) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_BOOT_CRD) );
lcdc.c:               LCDprintf( LCD_C, 0, LCDStr(__LCD_CREATE_PCMCIA) );
lcdc.c:           LCDprintf( LCD_C, 0, LCDStr(__LCD_CREATE_PCMCIA) );
lcdc.c:       LCDprintf( LCD_L, 0, LCDStr(__LCD_MAIN_MENU_SCRN) );
lcdc.c:       LCDprintf( LCD_R, 3, TextStr_8(__NEXT_ARW) );
lcdc.c:       LCDprintf( LCD_L, 3, TextStr_8(__ARW_EXIT) );
lcdc.c: int   LCDprintf( int x, int y, char *fmt, ... )
ocup.c:        LCDprintf( 0, 1, "%s",LCDStr(__PLS_PRESENT_CRD) );
ocup.c:        LCDprintf( 0, 2, "%s",LCDStr(__LCD_FOR_RECH) );
ocup.c:               LCDprintf( 0, 1, "%s",LCDStr(__CLR_FEED) );
ocup.c:                      LCDprintf( 0, 1, "%s",LCDStr(__RST_VAL) );
ocup.c:                   LCDprintf( 0, 1, "%s",LCDStr(__VAL_OOS) );
ocup.c:             LCDprintf( 0, 1, "%s",LCDStr(__VAL_OK) );
ocup.c:    LCDprintf( LCD_L, 1, LCDStr(__LCD_CRD_INFO_DISP) );
ocup.c:    LCDprintf( LCD_R, 1, "%8s", ((TRIM_DISPLAY_CARD*)p)->display );
ocup.c:    LCDprintf( LCD_L, 3, LCDStr(__LCD_CRD_INFO_EXP) );
ocup.c:    LCDprintf( LCD_R, 3, "%02d/%02d/%02d",
ocup.c:          LCDprintf( LCD_L, 0,            LCDStr(__LCD_CRD_INFO_TYPE) );
ocup.c:          LCDprintf( LCD_R, 0,            TextStr_8(__PERIOD) );
ocup.c:          LCDprintf( LCD_L, 2,            LCDStr(__LCD_CRD_INFO_REM) );
ocup.c:          LCDprintf( LCD_R, 2,            TextStr_8(__UNLIMITED) );
ocup.c:          LCDprintf( LCD_L, 0,            LCDStr(__LCD_CRD_INFO_TYPE) );
ocup.c:          LCDprintf( LCD_R, 0,            TextStr(__CHNG_VAL) );
ocup.c:          LCDprintf( LCD_L, 2,            LCDStr(__LCD_CRD_INFO_REM) );
ocup.c:          LCDprintf( LCD_R, 2, "$%06.2f", (float)((TRIM_DISPLAY_CARD*)p)->value/100);
ocup.c:          LCDprintf( LCD_L, 0,              LCDStr(__LCD_CRD_INFO_TYPE) );
ocup.c:          LCDprintf( LCD_R, 0,              TextStr(__MULT_RIDE) );
ocup.c:            LCDprintf( LCD_L, 2,            LCDStr(__LCD_CRD_INFO_REM) );
ocup.c:            LCDprintf( LCD_R, 2, "%02d %s", ((TRIM_DISPLAY_CARD*)p)->value, 
ocup.c:            LCDprintf( LCD_L, 2,            LCDStr(__LCD_CRD_INFO_REM) );
ocup.c:            LCDprintf( LCD_R, 2, "%02d %s", ((TRIM_DISPLAY_CARD*)p)->value, 
ocup.c:          LCDprintf( LCD_L, 0,              LCDStr(__LCD_CRD_INFO_TYPE) );
ocup.c:          LCDprintf( LCD_R, 0, "%s",        TextStr(__MAINT_DVR) );
ocup.c:          LCDprintf( LCD_L, 0,              LCDStr(__LCD_CRD_INFO_TYPE) );
ocup.c:          LCDprintf( LCD_R, 0,              TextStr_8(__TRANSFER) );
ocup.c:          LCDprintf( LCD_L, 2,              LCDStr(__LCD_CRD_INFO_RDS_USD) );
ocup.c:          LCDprintf( LCD_R, 2, "%8d",       ((TRIM_DISPLAY_CARD*)p)->xfer_trip );
ocup.c:          LCDprintf( LCD_L, 0,              LCDStr(__LCD_CRD_INFO_TYPE) );
ocup.c:          LCDprintf( LCD_R, 0,              TextStr_8(__UNKNOWN) );
ocup.c:           LCDprintf( LCD_L, 3, LCDStr(__LCD_CRD_INFO_EXP) );
ocup.c:           LCDprintf( LCD_R, 3, "%02d/%02d/%02d", tm->tm_mon+1, tm->tm_mday, (tm->tm_year % 100) );
prbp.c:    LCDprintf( 6, 1, "%s", b );
scpr.c:             LCDprintf(     0, 1, "%s",     LCDStr(__LCD_VALUE) );
scpr.c:             LCDprintf( LCD_R, 1, "$%5.2f", (float)SC_Card.RemVal/100.0 ); /* display */ 
scpr.c:             LCDprintf(     0, 1, "%s",     LCDStr(__LCD_RIDES) );
scpr.c:             LCDprintf( LCD_R, 1, "%d",     SC_Card.RemVal ); /* display */ 
scpr.c:          LCDprintf( LCD_L, 1, "%s",LCDStr(__PLS_PRESENT_CRD) );
scpr.c:          LCDprintf( LCD_L, 2, "      %08ld      ", SmartCD_Seq );
scpr.c:          LCDprintf( LCD_L, 3, "%s",LCDStr(__LCD_FOR_RECH) );
scpr.c:          LCDprintf( LCD_L, 1, "%s", LCDStr(__LCD_AMT_OWED) ); 
scpr.c:          LCDprintf( LCD_R, 1, "$%5.2f", (float)(RechargeVal-Driver_Rev)/100.0 ); /* display */ 
scpr.c:          LCDprintf( 0, 2, "%s", LCDStr(__LCD_INSERTED) );
scpr.c:          LCDprintf( LCD_R, 2, "$%5.2f", (float)Driver_Rev/100.0 ); /* display                 */ 
scpr.c:       LCDprintf( 0, 1, "%s", LCDStr(__LCD_RECH_COMP) );
sscp.c:            LCDprintf( 0, 1, "%s", LCDStr(__PLS_PRESENT_CRD) );
sscp.c:              LCDprintf( 0, 2, "      %08ld      ", SmartCD_Seq );
sscp.c:              LCDprintf( 0, 3, "%s", LCDStr(__LCD_SAT_FARE) );
sscp.c:              LCDprintf( 0, 2, " %ld %s", SmartCD_Seq, TextStr(__FOR_RECH) );
sscp.c:         LCDprintf( 0, 1, "%s", LCDStr(__LCD_NO_SERV_AVAIL) );
sscp.c:         LCDprintf( 0, 2, "%s", LCDStr(__LCD_FOR_RECH) );
sscp.c:     LCDprintf(     0, 0, "%s", LCDStr(__LCD_SC_ID) );
sscp.c:     LCDprintf( LCD_R, 0, "%5u", SerNum );
stat.c:       LCDprintf( 4, 1, "%s", TextStr(__UPLOAD_OCU) );
stat.c:      LCDprintf( 3, 1, "%s", TextStr(__DOWNLOAD_OCU) );
stat.c:    LCDprintf( 3, 2, " %s %02d%%  ", TextStr_8(__PERCENT), OCUPercent() ); 
version.c: |           04/08/05, aat - in LCDprintf() if language setting from Data      |
version.c: |                           called from oprintf() and LCDprintf() if the      |
