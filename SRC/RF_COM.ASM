*-----------------------------------------------------------------------------+
*  Src File:   rf_com.asm                                                       |
*  Version :   1.05                                                           |
*  Authored:   01/11/96, tgh                                                  |
*  Function:   Low level Rf comm. connector                                   |
*  Comments:                                                                  |
*                                                                             |
*  Revision:                                                                  |
*      1.00:   01/11/96, tgh  -  Initial release.                             |
*      1.01:   10/30/98, sjb  -  modified to talk to dcu                      |
*      1.02:   11/02/98, sjb  -  modified to talk to dcu & coin mech          |
*      1.03:   02/19/04, sjb  -  modify to run quart at full speed            |
*      1.04:   03/23/04, sjb  -  remove read of scsr register in txmode       |
*      1.05:   03/17/05, sjb  -  check THR before exiting irq to see if ready |
*                                for another character to send                |
*                                                                             |
*           Copyright (c) 1993-1996  GFI/USPS All Rights Reserved             |
*-----------------------------------------------------------------------------+

*-----------------------------------------------------------------------------+
*                       Include Files                                         |
*-----------------------------------------------------------------------------+

        include gen.inc
        include regs.inc
        include comm.inc
        include quart.inc

*-----------------------------------------------------------------------------+
*                       Local Equates                                         |
*-----------------------------------------------------------------------------+

*       Hardware dependant equates :

        quart_base:     equ     quart2base      base address
        quart_port_a:   equ     0               define as 1 if port A
        quart_port_b:   equ     0               define as 1 if port B
        quart_port_c:   equ     1               define as 1 if port C
        quart_port_d:   equ     0               define as 1 if port D

* Note: these values are for a divided clock

*        B4800:          equ     10011001b       4800   baud (ACR[7]=1,X=0 or 1)
*        B9600:          equ     10111011b       9600   baud (ACR[7]=1,X=0 or 1)
*        B19200:         equ     11011101b       19.2K  baud (ACR[7]=1,X=0)
*        B38400:         equ     11001100b       38.4K  baud (ACR[7]=1,X=1)
*        B57600:         equ     01110111b       57.6K  baud (ACR[7]=1,X=1)
*        B115200:        equ     10001000b       115.2K baud (ACR[7]=1,X=1)

* Note: these values are for a non-divided clock

        B4800:          equ     10001000b       4800   baud (ACR[7]=1,X=0 )
        B9600:          equ     10011001b       9600   baud (ACR[7]=1,X=0 or 1)
        B19200:         equ     10111011b       19.2K  baud (ACR[7]=1,X=0 or 1)
        B38400:         equ     11001100b       38.4K  baud (ACR[7]=1,X=0)
        B57600:         equ     01100110b       57.6K  baud (ACR[7]=1,X=1)
        B115200:        equ     01110111b       115.2K baud (ACR[7]=1,X=1)
        B230400:        equ     10001000b       230.4K baud (ACR[7]=1,X=1)

        BAUDRATE:       equ     B9600
**         BAUDRATE:       equ     B38400
*        BAUDRATE:       equ     B19200

  ifne quart_port_a

        imr_value:      equ     quart2_imr1_value location of imr value
        imr:            equ     imr1            fixme
        isr:            equ     isr1            fixme
        control_reg:    equ     cra             control register
        clk_sel_reg:    equ     csra            clock select register
        mode_reg:       equ     mra             mode selecet register
        rx_reg:         equ     rhra            receiver holding register
        tx_reg:         equ     thra            transmitter holding register
        acr:            equ     acra            Auxiliary Control Register A
        .tx_irq:        equ     0               transmit interrupt
        .rx_irq:        equ     1               receiver interrupt
        .delta_brk_irq: equ     2               delta break interrupt
        .counter_irq:   equ     3               counter interrupt
        status_reg:     equ     sra             status register

  endc

  ifne quart_port_b

        imr_value:      equ     quart2_imr1_value location of imr value
        imr:            equ     imr1
        isr:            equ     isr1
        control_reg:    equ     crb             control register
        clk_sel_reg:    equ     csrb            clock select register
        mode_reg:       equ     mrb             mode selecet register
        rx_reg:         equ     rhrb            receiver holding register
        tx_reg:         equ     thrb            transmitter holding register
        acr:            equ     acra            Auxiliary Control Register A
        .tx_irq:        equ     4               transmit interrupt
        .rx_irq:        equ     5               receiver interrupt
        .delta_brk_irq: equ     6               delta break interrupt
        .counter_irq:   equ     3               counter interrupt
        status_reg:     equ     srb             status register

  endc

  ifne quart_port_c

        imr_value:      equ     quart2_imr2_value location of imr value
        imr:            equ     imr2
        isr:            equ     isr2
        control_reg:    equ     crc             control register
        clk_sel_reg:    equ     csrc            clock select register
        mode_reg:       equ     mrc             mode selecet register
        rx_reg:         equ     rhrc            receiver holding register
        tx_reg:         equ     thrc            transmitter holding register
        acr:            equ     acrb            Auxiliary Control Register B
        .tx_irq:        equ     0               transmit interrupt
        .rx_irq:        equ     1               receiver interrupt
        .delta_brk_irq: equ     2               delta break interrupt
        .counter_irq:   equ     3               counter interrupt
        status_reg:     equ     src             status register

  endc

  ifne quart_port_d

        imr_value:      equ     quart2_imr2_value location of imr value
        imr:            equ     imr2
        isr:            equ     isr2
        control_reg:    equ     crd             control register
        clk_sel_reg:    equ     csrd            clock select register
        mode_reg:       equ     mrd             mode selecet register
        rx_reg:         equ     rhrd            receiver holding register
        tx_reg:         equ     thrd            transmitter holding register
        acr:            equ     acrb            Auxiliary Control Register B
        .tx_irq:        equ     4               transmit interrupt
        .rx_irq:        equ     5               receiver interrupt
        .delta_brk_irq: equ     6               delta break interrupt
        .counter_irq:   equ     3               counter interrupt
        status_reg:     equ     srd             status register

  endc


        RX_BUF_SIZE:    equ     2048
        QUEUE_SIZE:     equ     2048

*-----------------------------------------------------------------------------+
*                       Local Macros                                          |
*-----------------------------------------------------------------------------+
*       Common Exit
        exit_interrupt: macro
                        jmp     irq_exit
                        endm

*       Zero flag set if tx is idle.
        tx_idle:        macro
                        move.l  a0,-(sp)        save register used
                        movea.l tx_id,a0        check state
                        cmpa.l  #tx_disable,a0  .
                        move.l  (sp)+,a0        restore register used
                        endm

*       Define the next transmit state
        next_tx:        macro
                        lea.l   \1,a1
                        move.l  a1,tx_id
                        endm

*       Define the next receive state
        next_rx:        macro
                        lea.l   \1,a1
                        move.l  a1,rx_id
                        endm

*       Set rx & tx to default states
        default_states: macro
                        next_rx getc
                        next_tx tx_disable
                        endm

*       Read a quart register leaving result in reg. d0
*       usage: getreg reg
*       reg   - register offset
        getreg:         macro
                        move.b  quart_base+\1,d0
                        endm

*       Write the contents on reg. d0 to a quart register.
*       usage: putreg reg,value
*       reg   - register offset
*       value - byte to send
        putreg:         macro
                ifnc '\2',''
                        move.b  \2,d0
                endc
                        move.b  d0,quart_base+\1
                        endm


*       Get this quart's imr register contents
        get_imr:        macro
                        move.b  imr_value,d0
                        endm

*       Put this quart's imr register contents and update common memory
*       usage: put_imr value
        put_imr:        macro
                        putreg  imr,\1
                        move.b  d0,imr_value
                        endm

*       Transmit a byte
*       usage: transmit value
        transmit:       macro
                        putreg  tx_reg,\1
                        endm


*       Enable the transmitter
        enable_tx:      macro
                        get_imr
                        bset    #.tx_irq,d0
                        put_imr
                        endm

*       Disable the transmitter
        disable_tx:     macro
                        get_imr
                        bclr    #.tx_irq,d0
                        put_imr
                        endm

*       Copy n bytes into queue
*       a0 -> source
*       d1 -  holds count
        _ncpy:          macro
                        movea.l q_front,a1      a1 -> destination
                next\@: move.b  (a0)+,(a1)+     move to destination buffer
                        cmpa.l  #q_end,a1       check for wrap
                        blo.s   no_wrap\@       no, continue
                        movea.l #q,a1           else, wrap-around
                no_wrap\@:
                        dbra    d1,next\@       update/check byte counter
                        move.l  a1,q_front      save pointer
                        endm

*       Copy NULL terminated string into queue
*       a0 -> source
        _cpy:           macro
                        movea.l q_front,a1      a1 -> destination
                next\@: move.b  (a0)+,d0        get source byte
                        bne.s   not_null\@
                        jmp     exit\@          NULL, get out
                not_null\@:
                        move.b  d0,(a1)+        save in destination buffer
                        cmpa.l  #q_end,a1       check for wrap
                        blo.s   no_wrap\@
                        movea.l #q,a1
                no_wrap\@:
                        jmp     next\@
                exit\@:
                        movea.l a1,q_front
                        endm

*       Pointer is in "a1"
*       usage: _bump_ptr <ptr>
        _bump_ptr:      macro  ptr
                        addq.l  #1,a1           bump a1
                        cmpa.l  #q_end,a1       check for wrap-around
                        blo.s   ok\@
                        movea.l #q,a1           wrap-around
                ok\@:   move.l  a1,\1
                        endm


*-----------------------------------------------------------------------------+
*                       Data Segment (local data)                             |
*-----------------------------------------------------------------------------+
*                section udata,,"data"           uninitialized data
                section xdata,,"xdata"           uninitialized data

                        ds.b    0  word align

        q_front:        ds.l    1               tx queue front pointer
        q_back:         ds.l    1               tx queue back pointer
        q:              ds.b    QUEUE_SIZE      tx queue
        q_end:

        tx_id:          ds.l    1               tx state

        rx_id:          ds.l    1               rx state
        rx_buf:         ds.b    RX_BUF_SIZE     receive buffer
        rx_ndx:         ds.l    1               index into rx buffer
        rx_bck:         ds.l    1               pointer to back of buffer
        rx_cnt:         ds.w    1               char. count in buffer

*---------------------------------------------------------------------------+
*                       External References                                 |
*---------------------------------------------------------------------------+
* external data
        xref    quart2_imr1_value
        xref    quart2_imr2_value

*-----------------------------------------------------------------------------+
*                       Code Segment                                          |
*-----------------------------------------------------------------------------+
                section S_ProcessRFComm,,"code"


        _fnct   ProcessRFComm                called from "quarts.asm"

                getreg  status_reg              check status reg. for errors
                andi.b  #01010000b,d0           .
                beq     irq_id                  no errors, process interrupt

                putreg  control_reg,#01000000b  reset error status

        irq_id:
                getreg  isr                     d0 = Interrupt Status
                and.b   imr_value,d0            check enabled ones only
                btst    #.rx_irq,d0             Receive Interrupt ?
                beq.s   check_tx
                getreg  rx_reg                  get char from holding reg.
                bra.s   getc                    and perform next function

        check_tx:
                btst    #.tx_irq,d0             Transmit Interrupt ?
                beq.s   check_delta_break
                movea.l tx_id,a1                get the current receive state
                jmp     (a1)                    and perform next function

        check_delta_break:
                btst    #.delta_brk_irq,d0      Delta Break Interrupt ?
                beq.s   check_end               no, get out
                putreg  control_reg,#01010000b  reset flag

        check_end:
                exit_interrupt


*---------------------------------------------------------------------------+
*                       Receive States                                      |
*---------------------------------------------------------------------------+
        getc:
                movea.l rx_ndx,a1               get index
                move.b  d0,(a1)+                store character
                move.l  a1,rx_ndx               store index
                move.w  rx_cnt,d0               get count
                addq.w  #1,d0                   bump count
                cmpi.w  #RX_BUF_SIZE,d0         check range
                bls.s   rx_cnt_ok               ok, continue
                movea.l rx_bck,a1               get back pointer
                addq.l  #1,a1                   bump count
                cmpa.l  #rx_buf+RX_BUF_SIZE,a1  check it
                blo.s   rx_bck_ok               ok, continue
                movea.l #rx_buf,a1              no, wrap-around
           rx_bck_ok:
                move.l  a1,rx_bck
                move.w  #RX_BUF_SIZE,d0         rx_cnt = maximum value
           rx_cnt_ok:
                move.w  d0,rx_cnt
                movea.l rx_ndx,a1               check index
                cmpa.l  #rx_buf+RX_BUF_SIZE,a1
                blo.s   rx_ndx_ok               ok, continue
                movea.l #rx_buf,a1              no, wrap-around
                move.l  a1,rx_ndx
           rx_ndx_ok:

                getreg  status_reg              more data ?
                andi.b  #00000011b,d0           .
                beq.s   rx_flag_data
                getreg  rx_reg                  get char from holding reg.
                bra.s   getc                    and queue it up

           rx_flag_data:
                _flag_comm_rx COMM_RF
                exit_interrupt

*---------------------------------------------------------------------------+
*                       Transmit States                                     |
*---------------------------------------------------------------------------+
   qputs:
                movea.l q_back,a1               get pointer
                cmpa.l  q_front,a1              any data ?
                beq.s   qputs_fini
                transmit (a1)                   transmit data it points to
                _bump_ptr q_back                update pointers
                cmpa.l  q_front,a1              check for finish
                beq.s   qputs_fini
                getreg  status_reg              more data ?
                andi.b  #00000100b,d0           .
                bne.s   qputs                   branch if yes
                exit_interrupt

        qputs_fini:
                next_tx tx_disable

   tx_disable:
                disable_tx

        irq_exit:
             rts


*---------------------------------------------------------------------------+
*   Function:   Transmit a NULL terminated string without adding new line.  |
*   Synopsis:   int error = RS422Puts( char *s )                            |
*   Input   :   char *s    - points to NULL terminated string.              |
*   Output  :   int  error - 0 successful, -1 error(s)                      |
*   Comments:                                                               |
*---------------------------------------------------------------------------+
        _fnct   _RFPuts
                link    a6,#0                   set up frame
                movem.l a0/a1,-(sp)             save registers used
                movea.l 8(a6),a0                a0 -> source
                _cpy                            copy block into queue
                next_tx qputs                   update tx state
                enable_tx                       let'em go
                clr.w   d0                      return ok
                bra.s   puts_exit               exit

           puts_err:
                move.w  #-1,d0                  return error

        puts_exit:
                movem.l (sp)+,a0/a1             restore registers used
                unlk    a6                      restore frame
             rts                                return to caller


*---------------------------------------------------------------------------+
*   Function:   Transmit an N byte string without adding new line.          |
*   Synopsis:   int error = RS422Nputs( char *s, int n )                    |
*   Input   :   char *s    - points to NULL terminated string.              |
*           :   int  n     - number of bytes to transmit.                   |
*   Output  :   int  error - 0 successful, -1 error(s)                      |
*   Comments:                                                               |
*---------------------------------------------------------------------------+
        _fnct   _RFNputs
                link    a6,#0                   set up frame
                movem.l d1/a0/a1,-(sp)          save registers used
                movea.l 8(a6),a0                a0 -> source
                move.w  12(a6),d1               d1 -  number of bytes
                subq.w  #1,d1                   ajust byte count
                bmi.s   nputs_err               and check range
                _ncpy                           copy block into queue
                next_tx qputs                   update tx state
                enable_tx                       let'em go
                clr.w   d0                      return ok
                bra.s   nputs_exit              exit

           nputs_err:
                move.w  #-1,d0                  return error

        nputs_exit:
                movem.l (sp)+,d1/a0/a1          restore registers used
                unlk    a6                      restore frame
             rts                                return to caller


*---------------------------------------------------------------------------+
*   Function:   Transmit a character (low level routine)                    |
*   Synopsis:   int error = RS422Putc( char c )                             |
*   Input   :   char c     - character to transmit.                         |
*   Output  :   int  c     - returns character to caller.                   |
*   Comments:                                                               |
*---------------------------------------------------------------------------+
        _fnct   _RFPutc
                link    a6,#0                   setup stack frame
                movem.l a1,-(a7)                save registers used
                movea.l q_front,a1              get pointer
                move.b  9(a6),(a1)              store character in queue
                _bump_ptr q_front               update pointers
                next_tx qputs                   update transmit state
                enable_tx                       and let'em go
                movem.l (a7)+,a1                restore registers used
                move.b  4(a6),d0                return character to caller
                unlk    a6                      restore frame
             rts


*---------------------------------------------------------------------------+
*   Function:   Get a character (low level routine)                         |
*   Synopsis:   int error = RS422Getc( void )                               |
*   Input   :   None.                                                       |
*   Output  :   int  c     - returns character to caller.                   |
*   Comments:   Sytem timers are scanned within this function.              |
*---------------------------------------------------------------------------+
        _fnct   _RFGetc

                movem.l a0,-(a7)                save registers used

           getc_wait:
                tst.w   rx_cnt                  check for character(s)
                bne.s   getc_ready              character ready
                move.b  #0,d0                   return NULL
                bra.s   getc_exit               keep waiting

           getc_ready:
                move.w  rx_cnt,d0               check count
                movea.l rx_bck,a0               get index
                move.b  (a0)+,d0                get character
                cmpa.l  #rx_buf+RX_BUF_SIZE,a0  check pointers
                blo.s   s_g_rx_bck_ok           ok, continue
                movea.l #rx_buf,a0              else, wrap-around

           s_g_rx_bck_ok:
                move.l  a0,rx_bck               store back pointer
                subi.w  #1,rx_cnt               update count

           getc_exit:
                movem.l (a7)+,a0                restore registers used

             rts


*---------------------------------------------------------------------------+
*   Function:   Check to see if a character is pending.                     |
*   Synopsis:   int n = RS422Cnt( void )                                    |
*   Input   :   None.                                                       |
*   Output  :   int  n     - returns the number of characters pending.      |
*   Comments:                                                               |
*---------------------------------------------------------------------------+
        _fnct   _RFCnt
                move.w  rx_cnt,d0               return count in d0
             rts


*---------------------------------------------------------------------------+
*   Function:   Initialize the RS422 port.                                  |
*   Synopsis:   int error = InitializeRS422Comm( void )                     |
*   Input   :   baud rate.                                                       |
*   Output  :   int error  - returns 0 (OK)                                 |
*   Comments:   19200 baud                                                  |
*---------------------------------------------------------------------------+
        _fnct   _InitializeRFComm

                link    a6,#0                   setup stack frame
                movem.l d1/a0/a1,-(a7)          save registers used

                clr.w   rx_cnt                  clear rx byte count
                movea.l #rx_buf,a0              setup rx queue pointers
                move.l  a0,rx_ndx
                move.l  a0,rx_bck

                movea.l #q,a0                   setup tx queue pointers
                move.l  a0,q_front
                move.l  a0,q_back

                default_states                  ; set rx & tx states

                putreg  control_reg,#00101010b  reset rxda
                putreg  control_reg,#00110000b  reset txda
                putreg  control_reg,#01000000b  reset error status
                get_imr                         get current imr value
                bset    #.rx_irq,d0             enable rx interrupts
                put_imr
                putreg  control_reg,#00010000b  reset, point to reg. 1
                putreg  mode_reg,#00010011b     char, 8, none
                putreg  mode_reg,#00000111b     normal mode, 1 stop bit
                putreg  acr,#10000000b          baud rate divisor (no divisor)
                move.b  9(a6),d1                d1 = baudrate argument
                cmpi.b  #0,d1                   zero means use default
                bne.s   check_baud
                move.b  #BAUDRATE,d1            use default baudrate

            check_baud:             
                cmpi.b  #B38400,d1              baud rate at 38400 ?     
                beq.s   clear_ACR
                putreg  control_reg,#10000000b  set Rx extend bit
                putreg  control_reg,#10100000b  set Tx extend bit
                bra.s   set_baud_Rate

            clear_ACR: 
                putreg  control_reg,#10010000b  clear Rx extend bit
                putreg  control_reg,#10110000b  clear Tx extend bit

            set_baud_Rate:
                putreg  clk_sel_reg,d1          defined at top of module
                putreg  control_reg,#00000101b  enable transmitter & receiver

                movem.l (a7)+,d1/a0/a1          restore registers used
                unlk    a6                      restore frame
                clr.w   d0                      return zero

             rts
