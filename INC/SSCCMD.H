/*----------------------------------------------------------------------------\
|  Src File:   SSCcmd.h                                                       |
|  Authored:   07/13/01, aat                                                  |
|  Function:                                                                  |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:                                                                  |
|                                                                             |
|           Copyright (c) 2001  GFI All Rights Reserved                       |
\----------------------------------------------------------------------------*/
#ifndef  _SSCCMD_H
#define  _SSCCMD_H

/* System headers
*/
#include <stdio.h>

/* Project headers
*/


/*******************************************
**                                        **
** R/W'er Information Structure           **
**                                        **
*******************************************/
typedef struct {
	
	BYTE	system				[2];	/* system code */
	
	BYTE	cryption;					/* cryption */
	
	DWORD	timeout_ack;				/* timeout for waiting ack */
	DWORD	timeout;					/* timeout for waiting response */
	
	BYTE	idtr				[8];	/* idtr */
	BYTE	kytr				[8];	/* kytr */
	
	BYTE	equipment_code		[8];	/* equipment code */
	BYTE	system_parameter	[8];	/* system parameter */
	
	BYTE	key_ka				[8];
	BYTE	key_kb				[8];
	BYTE	key_version			[2];	/* key version */
	
} RWerInfo;


/*******************************************
**                                        **
** Card Information                       **
**                                        **
*******************************************/
typedef struct {
	BYTE	*system_code;
	
	BYTE	*area;
	BYTE	*area_key;
	BYTE	*area_keyversion;
	BYTE	area_number;
	
	BYTE	*service_code;
	BYTE	*service_key;
	BYTE	*service_keyversion;
	BYTE	service_number;
	
	BYTE	timeslot;
	
	BYTE	*idt;
	BYTE	*idtc;
	BYTE	*idi;
	BYTE	*kytc;
	
	BYTE	*idm;
	BYTE	*pmm;
	BYTE	*idmpmm;
	BYTE	*pmi;
	
	BYTE	*status;
	
	BYTE	mode;
	BYTE	result;
	
	BYTE	*key_group_master;
	BYTE	*key_group_access;
	
	BYTE	flag;
	
	BYTE	*number_blocks;
	BYTE	*block_list;
	BYTE	*block_data;
	BYTE	receive_number;
	
	BYTE	*issueid_block;
	
	BYTE	*package;
	
	BYTE	*function;
} CardInfo;


/*******************************************
**                                        **
** Reader/Writer Commands                 **
**                                        **
*******************************************/
typedef union {
/*
** Command:  00h
** Purpose:  Attention Command
** Usage:    Allows the Controller to recognize the 
**           presence of the R/W.
** Response: 01h
*/
	struct {
		BYTE system[2];
		
		BYTE equipment_code  [8];
		BYTE system_parameter[8];
		BYTE key_version     [2];
	} attention;
/*
** Command:  02h
** Purpose:  Authentication1 Command
** Usage:    Enables the Controller to authenticate the R/W 
**           during the first and second passes of the Mutual
**           Authentication procedure.
** Response: 03h
*/
	struct {
		BYTE system[2];
		BYTE equipment_code[8];
		BYTE m1r_a[8];
		
		BYTE m2r_b[8];
		BYTE m3r_b[8];
	} authentication1;
/*
** Command:  04h
** Purpose:  Authentication2 Command
** Usage:    Enables the R/W to authenticate the Controller
**           during the third and fourth passes of the Mutual
**           Authentication procedure.
** Response: 05h
*/
	struct {
		BYTE system[2];
		BYTE equipment_code[8];
		BYTE m4r_a[8];
		
		BYTE result;
	} authentication2;
	
/*
** Command:  40h
** Purpose:  Self Diagnostics Command
** Usage:    Executes self-diagnostic procedures within 
**           the R/W and returns the result
** Response: 41h
*/
	struct {
		BYTE diagnosis_number;     /* 00h - Communication Line Test           */
								   /* 01h - Flash Memory Test                 */
		                           /* 02h - RAM Test                          */
		                           /* 03h - CPU Function Test                 */
		                           /* 04h - Encrypt/Decrypt Function Test     */
		                           /* 10h - Polling Test to Card              */
		BYTE parameter_length;
		BYTE *parameter;
		
		BYTE result_length;
		BYTE *result;
	} self_diagnosis;
	
/*
** Command:  42h
** Purpose:  Firmware Maintenance Command
** Usage:    Updates the contents of the Reader/Writer
**           internal firmware.
** Response: 43h
*/
	struct {
		BYTE packet_number;
		BYTE number_remaining;
		BYTE length;
		BYTE *data;
		
		BYTE result;
	} firmware_maintenance;
	
/*
** Command:  44h
** Purpose:  Check Firmware Version Command
** Usage:    Checks the firmware version in the R/W'er.
** Response: 45h
*/
	struct {
		BYTE module_number;
		
		BYTE module_version;
		BYTE module_version_sub;
	} check_firmware_version;
	
	CardInfo *polling;							/* 80h */
	CardInfo *request_service;					/* 82h */
	CardInfo *request_response;					/* 84h */
	CardInfo *mutual_authentication;			/* 86h */
	CardInfo *read_block;						/* 88h */
	CardInfo *write_block;						/* 8ah */
	CardInfo *release;							/* 8eh */
	CardInfo *read_block_without_encryption;	/* 98h */
	CardInfo *write_block_without_encryption;	/* 9ah */
	
	CardInfo *register_issueid;					/* c0h */
	CardInfo *register_area;					/* c2h */
	CardInfo *register_service;					/* c4h */
	
	CardInfo *register_manufactureid;			/* e0h */
	
	CardInfo *card_self_diagnosis;
	
	CardInfo *card_read_process;
	CardInfo *card_write_process;
	
} CmdInfo;


#endif /* __SSCCMD_H_ */
