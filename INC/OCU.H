/*----------------------------------------------------------------------------\
|  Src File:   OCU.h                                                          |
|  Authored:   09/17/96, sjb                                                  |
|  Function:   Defines & prototypes for Operator Control Unit.                |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   06/28/99, sjb  -  Initial release.                             |
|                                                                             |
|           Copyright (c) 1993-1999  GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
#ifndef  _OCU_H
#define  _OCU_H

/* Defines
*/
/*** OCU Message types ***/
#define  OM_KEY         0xa0                    /* keyboard message */
#define  OM_DISP1       0xa1                    /* display line 1 */
#define  OM_DISP2       0xa2                    /* display line 2 */
#define  OM_ONLINE      0xa3                    /* unit online */
#define  OM_NUMDISP     0xa4                    /* numeric display */
#define  OM_STATUS      0xa5                    /* status msg/req */
#define  OM_SCREENCTRL  0xa6
#define  OM_KEYABORT    0xa7                    /* abort key sequence */
#define  OM_MENU        0xa8                    /* menu select */
#define  OM_DISPLAY     0xa9                    /* display a line */
#define  OM_LAMPON      0xaa                    /* turn backlite on  */
#define  OM_LAMPOFF     0xab                    /* turn backlite off */
#define  OM_REQSTATUS   0xac                    /* OCU status */
#define  OM_CLEARSCREEN 0xad                    /* clear the OCU display */
#define  OM_BOX2        0xae                    /* make a box based on x and y */
#define  OM_UNDEFINED   0xaf                    /* UNUSED */
#define  OM_TR_CNTL     0xb0                    /* block transfer control    */
#define  OM_TR_DATA     0xb1                    /* block transfer data       */
#define  OM_TR_LAST     0xb2                    /* block transfer last       */
#define  OM_REQ_PGM     0xb3                    /* program data control block (first) */
#define  OM_DATA_CNTL   0xb4                    /* program data control block (first) */
#define  OM_DATA_BLK    0xb5                    /* program data block        */
#define  OM_LAST_BLK    0xb6                    /* program last block (complete) */
#define  OM_KEYOPTION   0xb7                    /* blue key option */
#define  OM_ONOFF_LED   0xb9                    /* turn all leds on/off      */
#define  OM_BACK_STAT   0xbc                    /* ocu backup program status */
#define  OM_SOUND       0xbd

/*** OCU ID types ***/
#define  OID_ALL         (byte)'0'               /* broadcast                */
#define  OID_DCU         (byte)'1'               /* driver control unit      */
#define  OID_OCU         (byte)'1'               /* Operator control unit    */
#define  OID_PIU         (byte)'2'               /* passenger info. unit     */
#define  OID_FBX         (byte)'3'               /* farebox                  */
#define  OID_EXTSIGN     (byte)'4'               /* external sign            */
#define  OID_INTSIGN     (byte)'5'               /* internal sign            */
#define  OID_IR_PORT     (byte)'6'               /* internal test message    */
#define  OID_232PORT     (byte)'7'               /* internal test message    */

/* Screens
*/
/* Service screens "Screen" */
#define   MAIN_SCREEN          0
#define   LOGIN_SCREEN         1
#define   FARESEL_SCREEN       2
#define   FARESEL_SCREEN_2     3
#define   DIRSEL_SCREEN        4
#define   SETUP_SCREEN         5
#define   SETUP_SCREEN_2       6
#define   SETUP_SCREEN_3       7
#define   MAINT_SCREEN         8
#define   MAINT_SCREEN_2       9
#define   DATAREAD_SCREEN     10
#define   FRIENDLY_SCREEN     11
#define   FRIENDLY_SCREEN_2   12
#define   READ_CARD_SCREEN    13
#define   BILLOVERRIDE_SCREEN 14
#define   VERSION_SCREEN      15
#define   CONFIG_SCREEN       16
#define   IRMA_SCREEN         17
#define   PCMCIA_SCREEN       18
#define   IDLE_SCREEN         20
#define   MAINT_SCREEN_3      21
#define   COIN_TEST_SCREEN    22
#define   FBOX_STAT_SCREEN1   23
#define   FBOX_STAT_SCREEN2   24
#define   TRIM_DIAG_SCREEN    25


/* Driver Logon Menu "CurrentMenu" ***/
#define   LOGIN_ENTRY          0
#define   FARE_ENTRY           1
#define   DRIVER_ENTRY         2
#define   ROUTE_ENTRY          3
#define   RUN_ENTRY            4
#define   TRIP_ENTRY           5
#define   CITY_ENTRY           6
#define   DIR_ENTRY            7
#define   CONTROL_ENTRY        8
#define   MAINT_ENTRY          9
#define   MISC_ENTRY          10
#define   FRIENDLY_ENTRY      11
#define   PCCARD_ENTRY        12

/*** OCU Special Character Handling ***/
#define  SMALL_FONT           "\\S"
#define  BIG_FONT             "\\B"
#define  REVERSE_FONT         "\\R"
#define  PERCENT_SIGN         "%%"
#define  PERCNT_SIGN          "%"
#define  BLANK_NUMERIC        "      "

#define  OCU_REVSCREEN_ENB     1
#define  OCU_REVSCREEN_DIS     2
#define  OCU_NORMAL            3
#define  OCU_REVERSE           4

/*    terminal definitions    */
#define OCU_C         -1 /* OCU screen "auto" center text    */
#define OCU_T          0 /* OCU screen top reference         */
#define OCU_L          0 /* OCU screen left reference        */
#define OCU_R         30 /* OCU screen right reference       */
#define OCU_B         19 /* OCU screen bottom line reference */


#define  OCU_JUSTIFY           5
#define  OCU_LEFT              6
#define  OCU_CENTER            7
#define  OCU_RIGHT             8

#define  OCU_KEYREPEAT_DIS     0x01
#define  OCU_BLUE_OPTION       0x02


/* predefined function keys value referenced in
   media list as 2 key operation with group designator
*/
#define	  KEY_F1		0xcf	/* G15 */
#define	  KEY_F2		0xd0	/* G16 */
#define	  KEY_F3		0xd1	/* G17 */
#define	  KEY_F4		0xd2	/* G18 */
#define	  KEY_F5		0xd3	/* G19 */
#define	  KEY_F6		0xd4	/* G20 */
#define	  KEY_F7		0xd5	/* G21 */
#define	  KEY_F8		0xd6	/* G22 */

#define   O_BEEP        1
#define   O_WORBLE      2
#define   O_CHIRP       3
#define   O_BEEPONCE    4
#define   O_BOOP        5


/* 
 * Montreal leds
 */

#define    NO_LED         0x00
#define    R_RED          0x01
#define    R_YELLOW       0x02
#define    R_GREEN        0x04
#define    L_RED          0x08
#define    L_YELLOW       0x10
#define    L_GREEN        0x20

/* Maintenance codes of 1001 - 1005 are used by the Data
   System to signal when a component has been changed.
*/ 
#define  BILLMECH_SWAP     "    1001"
#define  BILLTRANS_SWAP    "    1002"
#define  COINMECH_SWAP     "    1003"
#define  TRIM_SWAP         "    1004"
#define  CARDREADER_SWAP   "    1005"

#define  CONTROL_NO        "   08470"
#define  FRIENDLIES        "   58395"
#define  DISPCONFIG        "   08558"
#define  J1708DIAGS        "   11111"
#define  TRIMDIAGS         "   22222"
#define  SCDIAGS           "   33333"
#define  TRIM_CLEAN        "   44444"

#define  FULLDISP          "  951413"

#define  COIN_VALIDATOR_TEST  "  AB85CD"
#define  TRIM_DIAGNOSTIC_TEST "  A8855B"

/* Pop-up Box Messages
*/
#define  NOT_ENOUGH     1

/* Typedef(s)
*/
typedef struct
{
word   farevalue[64][2];
int    fare_count;
}  MULTI_FARE;


/* Global data
*/
extern  MULTI_FARE       Multi_Fare;          /* read control structure   */

extern    int    CurrentMenu;
extern    int    LoginScreen;
extern    int    Data_Entered;     /* flag to indicate data changed to save data */
extern    int    FlashOn;
extern    int    MultiFare;       /* multifare selected */
extern    int    Xfer_Pend;
extern    int    OCU_Refresh;
extern    int    Screen;
extern    int    DisplayType;
extern    ulong  OCU_Part_No;
extern    word   OCU_ver;
extern    word   RS232Baud;
extern    int    PC_OCU_flag;
extern    int    Ctrl_Unit_Search;
extern    int    OCU_Status;
extern    int    EnterSerial;
extern    int    RechargeKey;
extern    int    PopBox;

/* Macros 
*/


/* Prototypes
*/
int   OCU_Msg          ( byte, byte, byte*, byte );  /* send OCU message */
int   OCU_KeyPush      ( int );                      /* process OCU key  */
void  OCU_state        ( int );                     /* change of state  */
void  OCU_Operate_Menu ( int );
void  OCU_MainScreen   ( void );
void  OCU_BlankDisplay ( int x, int y, int p, char *fmt, ... );
void  OCUErrorCode     ( int );
void  Main_Screen      ( int );
void  oprintf          ( int x, int y, char *fmt, ... );
void  CheckCBID        ( void );
void  obprintf         ( int x, int y, int x2, int y2, char *fmt, ... );
void  DisplayCard      ( void );    /*  */
void  RS232Ready       ( void );
void  RS232OnLine      ( void );
void  RS232OffLine     ( void );

void  CNFdsp           ( int );
int   InitializeRS232Comm( word );
int   InitializeCUBICProtocol( void );
void  ProcessCubic232  ( void );
int   OCU_Frames       ( void );
int   OCU_Download     ( long );
int   OCUPercent       ( void );
int   DisableRS232Comm ( word );
void  IRMA_Info        ( int  );
void  AutoxferDisp     ( int  );
void  DisplayCardData  ( void*, int );
void  FareboxStatus    ( void );
void  OcuSound         ( int tone, int duration, char *fmt, ... );
void  PopUpBox         ( int, int );

#endif /*_OCU_H*/

