/*----------------------------------------------------------------------------\
|  Src File:   pbq.h                                                          |
|  Authored:   09/20/94, ???                                                  |
|  Function:   Defines & prototypes for                                       |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   09/20/94, tgh  -  Initial release.                             |
|                                                                             |
|           Copyright (c) 1993, 1994 GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
#ifndef  _PBQ_H
#define  _PBQ_H


#include "mag.h"
#include "magcard.h"
#include "transact.h"
#include "scard.h"

#define NOT_FOUND (PBQ*)NULL
#define PBQ_SIG   0xF00D
#define PBQSIZE   100

typedef struct
{
  byte   Serial[10];            /* card's serial number                  */
  SCVDB0 image;                 /* card's image for restoring            */
  time_t time;                  /* time of original processing           */
  byte   fs;                    /* fareset of original processing        */
  byte   PBflag;                /* flag if no image stored               */
} PBQ;


extern word  pbq_sig;
extern int   QueuePass;
extern PBQ   Pbq[PBQSIZE];      /* passback queue memory                 */

void init_pbq( int flush );
PBQ* store_pbq( B_SERIAL *s, void *p, time_t t, byte fs );
PBQ* check_pbq( B_SERIAL *p );
void dq_pbq( void );
PBQ* delete_pbq( B_SERIAL *p );

#endif /*_PBQ_H*/
