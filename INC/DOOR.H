/*----------------------------------------------------------------------------\
|  Src File:   coin.h                                                         |
|  Authored:   03/04/96, sjb                                                  |
|  Function:   Defines & macros for processing coins.                         |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   03/04/96, sjb  -  Initial release.                             |
|           Copyright (c) 1994 - 1996 GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
#ifndef  _DOOR_H
#define  _DOOR_H

extern   int   DoorDump;

/* Define(s)
*/
#define         LRELOAD                  524

/* #define PROTOTYPE */

void Door_Open_Test(void);
int  Initialize_Lock_Oc2(void);
void Lid_Test(void);

#endif /*_DOOR_H*/

