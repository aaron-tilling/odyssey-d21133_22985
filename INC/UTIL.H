/*----------------------------------------------------------------------------\
|  Src File:   util.h                                                         |
|  Authored:   09/20/94, ???                                                  |
|  Function:   Defines & prototypes for utility functions.                    |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   09/20/94, tgh  -  Initial release.                             |
|                                                                             |
|           Copyright (c) 1993, 1994 GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
#ifndef  _UTIL_H
#define  _UTIL_H

#include  "gen.h"

/* getstr options */
#define _NUM    0x0001
#define _ALPHA  0x0002
#define _PUNCT  0x0004
#define _SPACE  0x0008
#define _STRUPR 0x0010
#define _XNUM   0x0020


#define  ATLOG_DEBUG_ON          (ATlog_Debug & 1)
#define  ATLOG_COMMANDS          (ATlog_Debug & 2)
#define  ATLOG_DATA              (ATlog_Debug & 4)

extern int    AT_Line;
extern int    CRC_Debug;
extern ulong  Crc_cs;
extern int    Crc_lp;


int  GetStr ( char *str, int n, int options );
int  Gets   ( char *str );
int  Ngets  ( char *str, int n );
int  Ngetn  ( char *str, int n );
int  Ngetx  ( char *str, int n );
int  Ngetsi ( char *str, int n );


char  asc2bcd( char a, char b );
void  ascii_hex_dump( char *addr, int n );
void  binary_dump( char *addr, int n );
void  binary_dump_rev( char *addr, int n );
void  xy_putc( int x, int y, char c );
void  xyprintf( int x, int y, char *fmt, ... );
void  ATlog( int x, int y, char *fmt, ... );
void  Background_Sort( void );
void  Background_CS( void );

void  crc_table( void );
word  crc16( char *ptr, long cnt );
byte  bcd( byte  num );
byte  bcd2bin( byte  num );
long  _a2l( char *p, int n );
long  _h2l( char *p, int n );

void  mem_test( void );
void  data_bus_test( void );
void  address_bus_test( void );
void  swab( char *from, char *to, int nbytes );

void  crc_table32( void );
ulong crc32( char *ptr, long cnt );
void  crc_32( char *ptr, long cnt );
ulong crc_32done( char *ptr, long cnt );

int   HexToBin( char*, byte* );

int   h2ascii_char( char *from, char *to, int nbytes );
int   _2ascii2h( char *from, char *to, int nbytes );

/*
** Cubic prototypes
*/
word Cubic_crc16             ( char *ptr, long cnt );
word Ascom_crc16             ( char *ptr, long cnt );
void Test_crc16              ( void );
int  ReplaceSpecialCharaters ( char String, char *TempBuf, int bytes );
#endif /*_UTIL_H*/
