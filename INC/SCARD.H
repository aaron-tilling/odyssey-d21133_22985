/*----------------------------------------------------------------------------\
|  Src File:   SCard.h                                                        |
|  Authored:   08/06/01, aat                                                  |
|  Function:                                                                  |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:                                                                  |
|                                                                             |
|           Copyright (c) 2001  GFI All Rights Reserved                       |
\----------------------------------------------------------------------------*/
#ifndef  _SCARD_H
#define  _SCARD_H

/* System headers
*/
#include <stdio.h>

/* Project headers
*/
#include "gen.h"
#include "magcard.h"
#include "rs485.h"
#include "transact.h"
#include "probe.h"

/* Defines
*/
#define  SCVDB1_offset      2     /* variable data packet 1 start offset   */
#define  SCVDB2_offset     25     /* variable data packet 2 start offset   */
#define  SCVDSZ            96     /* variable data max. size               */
#define  SC_SECURITY        5     /* smartcard security setting            */
#define  SCDATASZ          48     /* actual data size                      */

#define  SC_DEBUG        0x01
#define  SC_READ            3

#define  SC_RESP         SECONDS(2)    /* poll time */


/* check q1ac.asm if one of the following four change */
#define  SONY_READER        1
#define  OTI_SCI3000        2
#define  OTI_SCI1000        3
#define  ASCOM_READER       4
#define  GEM_PROX           5
#define  DISABLE_READER     8
#define  TRI_READER         9

#define  SONY_BAUD          B_19200
#define  OTI_DEFAULT_BAUD   B_9600
#define  OTI_FAST_BAUD      B_38400
#define  GEMP_DEFAULT_BAUD  B_57600
#define  GEMP_SLOW_BAUD     B_9600
#define  ASCOM_DEFAULT_BAUD B_4800
#define  ASCOM_FAST_BAUD    B_115200

#define  STX                0x02  /* Start of Message                     */
#define  ETX                0x03  /* End of Message                       */
#define  ACK                0x06  /* Message Acknowledgement - POSITIVE   */
#define  NAK                0x15  /* Message Acknowledgement - NEGATIVE   */

#define  SSC_DEBUG_ON            (SSC_Debug & 1)
#define  SSC_COMMANDS            (SSC_Debug & 2)
#define  SSC_DATA                (SSC_Debug & 4)
#define  SSC_RECOVER             (SSC_Debug & 8)
#define  SSC_TRIM_DEBUG          (SSC_Debug & 8)
#define  BACKUP_SIZE             8
#define  SC_SIGNATURE            0xfeedf00d

#define  STRUCTURE_0        0x00 /* Original "TRiM Magnetic" structure */
#define  STRUCTURE_1        0x01 /* New "SmartCard" structure w/backup copy */

/*
** structure pointers to Variable Data track buffer Block 1 and Block 2
** for format 0 - regular mag passes
*/
#define  SCVDB0_1 ( (SCVDB0*)((&scvd)) )
#define  SCVDB0_2 ( (SCVDB0*)((&scvd2)) )
#define  SCVDB0_3 ( (SCVDB0*)((&scvd3)) )

#define  SCVDB0A_1 ( (SCVDB0A*)((&scvd4)+SCVDB1_offset) )

/*
** structure pointers to Variable Data track buffer Block 1 and Block 2
** for format 1 - TRIM issued paper transfers
*/
#define SCVDB1_1 ( (SCVDB1*)((&scvd)) )
#define SCVDB1_2 ( (SCVDB1*)((&scvd2)) )

#define SCVDB1A_1 ( (SCVDB1A*)((&scvd4)+SCVDB1_offset) )

/*
** structure pointers to Variable Data track buffer Block 1 and Block 2
** for format 2 - Maintenance / Employee ID passes
*/
#define SCVDB2_1 ( (SCVDB2*)((&scvd)) )
#define SCVDB2_2 ( (SCVDB2*)((&scvd2)) )

#define SCVDB2A_1 ( (SCVDB2A*)((&scvd4)+SCVDB1_offset) )

/*
** structure pointers to Variable Data track buffer Block 1 and Block 2
** for format 3 - Swipe compatible period passes
*/
#define  SCVDB3_1 ( (SCVDB3*)((&scvd)) )
#define  SCVDB3_2 ( (SCVDB3*)((&scvd2)) )

#define  SCVDB3A_1 ( (SCVDB3A*)((&scvd4)+SCVDB1_offset) )

#define  TRIM_SC_ISSUE   0x0001
#define  TRIM_SC_READ    0x0002

/*   TrimSCflags &= ALL_TRIMSCFLAGS; */

extern word    TrimSCflags;

#define  ALL_TRIMSCFLAGS  (word)\
                          (TRIM_SC_ISSUE|TRIM_SC_READ)

#define  Set_TSCflag( f )          ( TrimSCflags |= (f) )
#define  Clr_TSCflag( f )          ( TrimSCflags &=~(f) )
#define  Tst_TSCflag( f )          ( TrimSCflags &  (f) )

/* Typedefs
*/

/* if structure changed, check data.c, it initializes all but
   C_ReadCount & C_WriteCount when probed
 */

typedef  struct
{
   word        ReadCount;
   word        ReadError;
   word        ReadBlockCount;
   word        ReadErrNoRecover;
   word        ReadRecovery;
   word        ReadRecoverError;
   word        WriteCount;
   word        WriteError;
   word        WriteBlockCount;
   word        WriteBlockError;
   word        offline;
   word        retry;
   word        com_err;
   ulong       C_ReadCount;
   ulong       C_WriteCount;
   long        Signiture;
}  SC_DIAG;

/* Global
*/
extern   int       IssueService;
extern   int       ReadSmartCard;
extern   int       WriteSmartCard;
extern   int       SC_Data_Dump;

extern ulong       SmartCD_Seq;
extern int         SC_Card_fare;

extern byte        scvd[SCVDSZ];  /* variable data buffer (read/write)     */
extern byte        scvd2[SCVDSZ]; /* variable data buffer (read/write)     */
extern byte        scvd3[SCVDSZ]; /* variable data buffer used for restore */
extern byte        scvd4[SCVDSZ]; /* variable data buffer used for restore */

extern CARD        SC_Card;
extern byte        SampleData[SCVDSZ];

extern byte        SSC_base;
extern byte        SSC_ver;
extern byte        TSC_base;
extern byte        TSC_ver;
extern ulong       TSC_ReaderSerial;
extern int         PrintOnSmartCards;

extern char        OTISC_ver[4];
extern char        GEMPSC_ver[4];

extern int         WipeoutCard;
extern int         CreateSmartCard;
extern int         EmbedXfer;
extern int         EmbededXfer;
extern int         EncodeSmartCard;

extern byte        S1Data[SCVDSZ];
extern byte        SC_Serial[4];
extern int         IssueSmartCard;

extern int         Startup;
extern int         SC_Baud;
extern int         BaudRateChange;
extern int         ReadSector;
extern int         UsedSC;
extern int         Structure;
extern int         OldFormat;
extern int         StartingBlock;
extern int         NumBlocks;
extern byte        KeyVer;

extern ulong       SC_ReaderSerial;

extern SC_DIAG     SC_Diag;

/*** AAT - For testing of Bad Listing Cards ***/
extern int         BadFlagSet;
extern int         UnBadList;

              /* SmartCard Ride/Value ticket format     */
typedef struct
{
  ulong Format       : 3;   /* Card Format Code - 0 (ride/value)    */
  ulong CardDesig    : 5;   /* Card Designator                      */
  ulong Group        : 1;   /* Group ( Ride or Value )              */
  ulong AgencyID     :12;   /* Card Initial Agency                  */
  ulong Threshold    :11;   /* "Auto re-charge threshold            */
  ulong SeqNo        :32;   /* Card sequence number (12 bits)       */
  word  StartDate    :16;   /* Date card becomes valid (unix days)  */
  word  Security     : 4;   /* Validation code                      */
  word  ExpOff       :12;   /* Number of days ticket is valid       */
  word  RechargeAmt  :16;   /* Init Ride/Val & amt to "Auto re-chrg"*/
  word  Spare_2      :12;   /*         Unused - 08-Dec-05           */
  word  Structure    : 4;   /* Structure to use ( for future )      */
/** END BLOCK 4 - Fixed Data 16 bytes **/
  word  ExpireDate   :16;   /* Date/time that card will expire.     */
  word  RemVal       :16;   /* Remaining value (trips, cents, etc.) */
  ulong BadFlag      : 1;   /* Badlist flag                         */
  ulong XferTrips    : 3;   /* TRANSFER - Trips used                */
  ulong IUDir        : 3;   /* TRANSFER - Initial use direction     */
  ulong SC_XferDesig : 5;   /* TRANSFER - Designator                */
  ulong IURoute      :20;   /* TRANSFER - Issuing Route             */
  ulong SC_XferExp   :32;   /* TRANSFER - Expiration Time           */
  word  IUFare       :10;   /* TRANSFER - Fare Paid                 */
  word  Spare_3      : 5;   /*         Unused - 08-Dec-05           */
  word  AutoRecharge : 1;   /* "Auto re-charge flag                 */
  word  CRC          :16;   /* Error detection code                 */
/** END BLOCK 5 - Variable Data 16 bytes ( duplicate on block 6 )  **/
} SCVDB0;

              /* SmartCard "Maintenance" ticket format     */
typedef struct
{
  ulong Format       : 3;   /* card format code - 2 (Maintenance)   */
  ulong CardDesig    : 5;   /* card designation (driver, supervisor)*/
  ulong Spare_1      : 1;   /*         Unused - 08-Dec-05           */
  ulong AgencyID     :12;   /* unique to each authority             */
  ulong SubGroup     :11;   /*         Unused - 08-Dec-05           */
  ulong Spare_2      :12;   /* sequential encodeing number          */
  ulong StartDate    :16;   /* date card becomes valid (unix days)  */
  ulong Security     : 4;   /* validation code                      */
  word  ExpOff       :12;   /* # of days card is valid              */
  word  MfgID        : 4;   /* manufacture's ID code                */
  word  PIN_Number   :16;   /* used with TVM/PEM                    */
  ulong EmpID        :24;   /* employee ID number                   */
  ulong Spare_4      : 4;   /*         Unused - 08-Dec-05           */
  ulong Structure    : 4;   /* Structure to use ( for future )      */
/** END BLOCK 4 - Fixed Data 16 bytes **/
  word  ExpireDate   :16;   /* Date/time that card will expire.     */
  word  Spare_5      :16;   /*         Unused - 08-Dec-05           */
  word  BadFlag      : 1;   /* Badlist flag                         */
  word  Usage        :10;   /* increments each time the card is used*/
  word  Spare_6      : 5;   /*         Unused - 08-Dec-05           */
  word  Spare_7      :16;   /*         Unused - 08-Dec-05           */
  ulong Spare_8      :32;   /*         Unused - 08-Dec-05           */
  word  Spare_9      :15;   /*         Unused - 08-Dec-05           */
  word  AutoRecharge : 1;   /* "Auto re-charge flag                 */
  word  CRC          :16;   /* error detection code                 */
/** END BLOCK 5 - Variable Data 16 bytes ( duplicate on block 6 ) **/
} SCVDB2;

              /* SmartCard "Swipe" ticket format     */
typedef struct
{
  ulong Format       : 3;   /* Card format code - 3 (swipe period)  */
  ulong CardDesig    : 5;   /* Card designation (driver, supervisor)*/
  ulong Spare_1      : 1;   /*         Unused - 08-Dec-05           */
  ulong AgencyID     :12;   /* Card Initial Agency                  */
  ulong Threshold    :11;   /* "Auto re-charge threshold            */
  ulong SeqNo        :24;   /* Sequential encodeing number          */
  ulong TypeExp      : 8;   /* type of expiration being used        */
  word  StartDate    :16;   /* Date card becomes valid (unix days)  */
  word  Security     : 4;   /* Validation code                      */
  word  ExpOff       :12;   /* Number of days ticket is valid       */
  word  MfgID        : 4;   /* Manufacture's ID code                */
  word  TPBCode      :12;   /* Third party billing code             */
  word  Spare_2      :12;   /* Time to add ( minuts - 1440 max )    */
  word  Structure    : 4;   /* Structure to use ( for future )      */
/** END BLOCK 4 - Fixed Data 16 bytes **/
  word  ExpireDate   :16;   /* Date/time that card will expire.     */
  word  Spare_3      : 4;   /*         Unused - 08-Dec-05           */
  word  AddTime      :12;   /* time to add on 24 hour & ADD_OFFSET  */
  word  BadFlag      : 1;   /* Badlist flag                         */
  word  Spare_4      :15;   /*         Unused - 08-Dec-05           */
  word  Spare_5      :16;   /*         Unused - 08-Dec-05           */
  ulong Spare_6      :32;   /*         Unused - 08-Dec-05           */
  word  Spare_7      :15;   /*         Unused - 08-Dec-05           */
  word  AutoRecharge : 1;   /* "Auto re-charge flag                 */
  word  CRC          :16;   /* Error detection code                 */
/** END BLOCK 5 - Variable Data 16 bytes ( duplicate on block 6 ) **/
} SCVDB3;


typedef struct              /* single track format                    */
{
  byte  Format      : 3;    /* card format code - 1 (TRIM transfers)  */
  byte  Desig       : 5;    /* transfer designation (local, express)  */
  ulong Agency      :12;    /* issueing agency ID                     */
  ulong Bus         :20;    /* issueing bus number                    */
  byte  Dir         : 3;    /* initial use direction                  */
  byte  Security    : 4;    /* validation code                        */
  byte  Spare1      : 1;    /* spare ( cancel receipt for LIMOCAR )   */
  word  SeqNo       :16;    /* sequence number                        */
  ulong Spare5      : 3;    /* spare field for future use             */
  ulong Trips       : 3;    /* transfer trips used                    */
  ulong PrintPos    : 6;    /* next available print line              */
  ulong Route       :20;    /* issueing route                         */
  ulong Expiration  :32;    /* transfer time of expiration            */
  word  Fare        :16;    /* fare paid for transfers                */
  word  CRC         :16;    /* error detection code                   */
} SCVDB1;


/** OLD STRUCTURES **/
typedef struct              /* SmartCard Ride/Value ticket format     */
{
  ulong Format      : 3;    /* ticket format code - 0                 */
  ulong XferDesig   : 5;    /* ticket designator                      */
  ulong ExpOff      :12;    /* number of days ticket is valid         */
  ulong IUAgency    :12;    /* ticket initial Agency                  */
  word  StartDate   :16;    /* date ticket becomes valid (unix days)  */
  word  RemVal      :16;    /* remaining value (trips, cents, etc.)   */
  ulong XferExp     :32;    /* ticket sequence number (12 bits)       */
  ulong XferTrips   : 3;    /*           currently unused             */
  ulong IUDir       : 3;    /*           currently unused             */
  ulong PrintPos    : 6;    /*           currently unused             */
  ulong IURoute     :20;    /*           currently unused             */
  word  spare1      : 5;    /* ticket group ( Ride or Value )         */
  word  BadFlag     : 1;    /* badlist flag                           */
  word  IUFare      :10;    /*           currently unused             */
  word  CRC         :16;    /* error detection code                   */
} SCVDB0A;


typedef struct              /* single track format                    */
{
  byte  Format      : 3;    /* card format code - 1 (TRIM transfers)  */
  byte  Desig       : 5;    /* transfer designation (local, express)  */
  ulong Agency      :12;    /* issueing agency ID                     */
  ulong Bus         :20;    /* issueing bus number                    */
  byte  Dir         : 3;    /* initial use direction                  */
  byte  Security    : 4;    /* validation code                        */
  byte  Spare1      : 1;    /* spare ( cancel receipt for LIMOCAR )   */
  word  SeqNo       :16;    /* sequence number                        */
  ulong Spare5      : 3;    /* spare field for future use             */
  ulong Trips       : 3;    /* transfer trips used                    */
  ulong PrintPos    : 6;    /* next available print line              */
  ulong Route       :20;    /* issueing route                         */
  ulong Expiration  :32;    /* transfer time of expiration            */
  word  Fare        :16;    /* fare paid for transfers                */
  word  CRC         :16;    /* error detection code                   */
} SCVDB1A;


typedef struct                   /* single track format                    */
{
  ulong Format      : 3;         /* card format code - 2 (Maintenance)     */
  ulong CardDesig   : 5;         /* card designation (driver, supervisor)  */
  ulong EmpID       :24;         /* employee ID number                     */
  word  Security    : 4;         /* validation code                        */
  word  AgencyID    :12;         /* unique to each authority               */
  word  MfgID       : 4;         /* manufacture's ID code                  */
  word  SeqNo       :12;         /* sequential encodeing number            */
  word  AbsExp      :16;         /* absolute expiration date               */
  word  Usage       :10;         /* increments each time the card is used  */
  word  PrintPos    : 6;         /* next available print line              */
  word  StartDate   :16;         /* date card becomes valid (unix days)    */
  ulong ExpOff      :12;         /* # of days card is valid                */
  ulong Spare2      :20;         /* spare field for future use             */
  word  CRC         :16;         /* error detection code                   */
} SCVDB2A;

typedef struct                   /* single track format                    */
{
  ulong Format      : 3;         /* card format code - 3 (swipe period)    */
  ulong CardDesig   : 5;         /* card designation (driver, supervisor)  */
  ulong SeqNo       :24;         /* sequential encodeing number            */
  word  Security    : 4;         /* validation code                        */
  word  AgencyID    :12;         /* unique to each authority               */
  word  MfgID       : 4;         /* manufacture's ID code                  */
  word  TPBCode     :12;         /* third party billing code               */
  word  AbsExp      :16;         /* absolute expiration date               */
  word  StartDate   :16;         /* date card becomes valid (unix days)    */
  ulong Spare1      :16;         /* spare field for future use             */
  ulong TypeExp     : 8;         /* type of expiration being used          */
  ulong IUFare      : 8;         /* spare (start/destination for LIMOCAR)  */
  word  BadFlag     : 1;         /* badlist flag                           */
  word  Spare2      : 3;         /* spare field for future use             */
  word  ExpOff      :12;         /* # of days card is valid                */
  word  CRC         :16;         /* error detection code                   */
} SCVDB3A;



/*
**  Serial number structure of format 0 cards
*/
typedef struct
{
  word   spare0      :16;
  ulong  Group       : 3;       /* card's group                          */
  ulong  Desig       : 5;       /* card's designation                    */
  ulong  SeqNoL      :24;       /* sequence number                       */
  word   Security    : 4;       /* security code                         */
  word   AgencyID    :12;       /* issuing agency                        */
  word   spare       : 8;
  word   SeqNoH      : 8;       /* sequence number high                  */
} SERIAL_SCF7;


/*
**  Serial number structure of format 1 cards
*/
typedef struct
{
  word   spare       :16;
  ulong  Group       : 3;       /* card's group                          */
  ulong  Desig       : 5;       /* card's designation                    */
  ulong  SeqNo       :24;       /* sequence number                       */
  word   BusH        : 4;       /* issuing bus number high               */
  word   AgencyID    :12;       /* issuing agency                        */
  word   BusL        :16;       /* issuing bus number low                */
} SERIAL_SCF5;



/*
**  Serial number structure of format 2 cards
*/
typedef struct
{
  word   spare       :16;
  ulong  Group       : 3;       /* card's group                          */
  ulong  Desig       : 5;       /* card's designation                    */
  ulong  EmpID       :24;       /* employee ID number                    */
  ulong  Security    : 4;       /* security code                         */
  ulong  AgencyID    :12;       /* issuing agency                        */
  ulong  MfgID       : 4;       /* manufaturer's ID code                 */
  ulong  SeqNo       :12;       /* sequence number                       */
} SERIAL_SCF2;

/*
**  Serial number structure of format 3 cards
*/
typedef struct
{
  word   spare       :16;
  ulong  Group       : 3;       /* card's group                          */
  ulong  Desig       : 5;       /* card's designation                    */
  ulong  SeqNo       :24;       /* sequence number                       */
  ulong  Security    : 4;       /* security code                         */
  ulong  AgencyID    :12;       /* issuing agency                        */
  ulong  MfgID       : 4;       /* manufaturer's ID code                 */
  ulong  TPBCode     :12;       /* third party billing code              */
} SERIAL_SCF0;

/* Prototype
*/
void    SCRegToCard            ( CARD *card, int, int );
void    SCXferToCard           ( CARD *card );
void    SCMaintToCard          ( CARD *card, int, int );
void    SCPerToCard            ( CARD *card, int, int );

/** USED FOR TRANSFERING TO NEW FORMAT **/
void    SCRegToCard_Old        ( CARD *card );
void    SCXferToCard_Old       ( CARD *card );
void    SCMaintToCard_Old      ( CARD *card );
void    SCPerToCard_Old        ( CARD *card );
void    Encode_Card            ( CARD *card );
/** **/

void    SCRegToSerial          ( void *serial );
void    SCXferToSerial         ( void *serial );
void    SCMaintToSerial        ( void *serial );
void    SCPerToSerial          ( void *serial );
void    CardToSCReg            ( SCVDB0 *v, CARD *card );
void    CardToSCXfer           ( SCVDB1 *v, CARD *card );
void    CardToSCMaint          ( SCVDB2 *v, CARD *card );
void    CardToSCPer            ( SCVDB3 *v, CARD *card );
ulong   ProcessSCRead          ( void );

void    Issue_Period           ( void );
void    IssueRide_Value        ( void );
int     GetSmartCard           ( CARD * card, int, int );
int     PutSmartCard           ( CARD *card );
void    SmartCardRestored      ( void );
void    SmartCardReceipt       ( void );
void    issue_employee         ( void );
void    InitializeQ1AComm      ( int );       /* proximity smartcard */
int     InitializeSonyProtocol ( void );
void    ProcessSony            ( void );
void    Complete_SC_Process    ( void );
void    Wipeout_SC             ( void );

int     InitializeASCOMProtocol( void );
void    ProcessAscom           ( void );
void    InitializeSmartCardProtocol ( void );
void    SC_Command             ( byte, int );


int   IssueRide_ValueSC( byte type, byte desig, word rev, void *s, int sz );
int   IssueTransferSC( byte type, byte desig, word rev, void *s, int sz );

int   SmartCardMsg             ( void*, int );
void  ReqSCInfo                ( void );

void  Issue_SC( byte type, byte desig, word rev, void *s, int sz );
void  Issue_RVSC( byte type, byte desig, word rev, void *s, int sz );

void  SC_RechargeChk       ( void );

#endif /* __SCARD_H_ */

