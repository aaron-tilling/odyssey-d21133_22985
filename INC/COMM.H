/*----------------------------------------------------------------------------\
|  Src File:   comm.h                                                         |
|  Authored:   01/11/96, tgh                                                  |
|  Function:   Communication process defines and macros for C modules.        |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   01/11/97, tgh  -  Initial release.                             |
|      1.01:   09/19/97, tgh  -  Added COMM_Q0A, COMM_Q1A.                    |
|      1.02:   03/05/98, sjb  -  Added COMM_TM (Dallas Touch Memory (DTM) )   |
|                                                                             |
|           Copyright (c) 1993-1997  GFI/USPS All Rights Reserved             |
\----------------------------------------------------------------------------*/
#ifndef  _COMM_H
#define  _COMM_H

/* Defines
*/
#define  COMM_232             0x0001      /* RS232 (touch panel)       q1b */
#define  COMM_DBG             0x0002      /* diagnostic (stdin/stdout) q0b */
#define  COMM_485             0x0004      /* RS485 TRIM communications q0c */
#define  COMM_PRB             0x0008      /* probe commnications       q0d */
#define  COMM_SCI             0x0010      /* SCI (1708) J1 pin 17,18   331 */
#define  COMM_Q0A             0x0020      /* bill validator            qoa */
#define  COMM_Q1A             0x0040      /* proximity smartcard       q1a */
#define  COMM_DTM             0x0080      /* Dallas Touch Memory (DTM) q1c */
#define  COMM_422             0x0100      /* RS422 (dcu/PPS)           q1d */
#define  COMM_RF              0x0200      /* RF  (dcu/PPS)             q2c */


extern   int    OLD_DALLAS;

/* Prototypes
*/
int   FlagCommRx( unsigned int );
int   DTM_RxIdle( byte );               /* protocol receive states */

#endif /* _COMM_H */
