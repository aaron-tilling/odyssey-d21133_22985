/*----------------------------------------------------------------------------\
|  Src File:   OTI.h                                                          |
|  Authored:   08/06/01, aat                                                  |
|  Function:                                                                  |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:                                                                  |
|                                                                             |
|           Copyright (c) 2001  GFI All Rights Reserved                       |
\----------------------------------------------------------------------------*/
#ifndef  _GEMP_H
#define  _GEMP_H

/* System headers
*/
#include <stdio.h>

/* Project headers
*/


/* Defines
*/
/** The following are defined in Scard.h **/
/* #define  STX    0x02  /* Start of Text              */
/* #define  ETX    0x03  /* End of Text                */
/* #define  ACK    0x06  /* Possitive Acknowledgement  */
/* #define  NAK    0x15  /* Negative Acknowledgement   */

#define  SC_MAX_RETRIES	       3
#define  SC_POLLTIME           SECONDS(3)      /* poll time */
/*#define  SC_RESPTIME           MSEC(300)     /* poll time */
#define  SC_POLLRSP            MSEC(500)       /* poll response time */
/*#define  SC_RESP               SECONDS(2)    /* poll time */



#define  NAD               0x42
#define  SAD               0x04 /* Reader Address */
#define  DAD               0x02 /* Farebox Address */

#define  I_BLOCK           0x00
#define  S_BLOCK           0xc0

#define  DEFAULT_KEYS      0x01

#define  MIFARE_CARD_4K    0x02
#define  MIFARE_CARD_1K    0x04
#define  MIFARE_ULTRALIGHT 0x44

#define  SERIAL_4_BYTE     0x87
#define  SERIAL_7_BYTE     0x8a
#define  SERIAL_10_BYTE    0x8d
 
/*
** Commands
*/
#define  GBP_PRE_CMD_E1               0xe1
#define  GBP_PRE_CMD_E2               0xe2
#define  GBP_PRE_CMD_E5               0xe5
#define  GBP_CLA_CMD_80               0x80
#define  GBP_CLA_CMD_84               0x84
#define  GBP_CLA_CMD_90               0x90
#define  GBP_CLA_CMD_94               0x94
#define  GBP_CLA_CMD_96               0x96


/* - I/O Management Commands
*/
#define  GBP_RG_LED_CTRL_CMD          0x20
#define  GBP_B_LED_CTRL_CMD           0x21
#define  GBP_BUZZER_CMD               0x33
#define  GBP_CONFIG_COM_CMD           0x0a
#define  GBP_SET_DELAY_CMD            0x23
#define  GBP_ACTIVATE_CASE_DETECT_CMD 0x37
#define  GBP_ACKNOWLEDGE_CASE_CMD     0x10

/* - Miscellaneous Commands
*/
#define  GBP_MODE_CMD                 0xe6
#define  GBP_READ_ROS_FIRMWARE_CMD    0xff
#define  GBP_READ_OROS_FIRMWARE_CMD   0x22
#define  GBP_READ_EEPROM_CMD          0x52
#define  GBP_WRITE_EEPROM_CMD         0x53
#define  GBP_GET_READER_ID_CMD        0x57

/* - ISO14443 RF Commands
*/
#define  GBP_RF_ON                    0x10
#define  GBP_RF_OFF                   0x11
#define  GBP_MOD_TYPE_CMD             0xea
#define  GBP_GETREADER_PARAMS_CMD     0xe0

/* - ISO14443 A Card Basic Commands
*/
#define  GBP_REQUEST_ALL_A_CMD        0x11
#define  GBP_REQUEST_A_CMD            0x10
#define  GBP_ANTICOLL_CMD             0x12
#define  GBP_SELECT_CMD               0x13
#define  GBP_HALTA_CMD                0x15
#define  GBP_REQ_ATS_CMD              0x16
#define  GBP_PPSR_CMD                 0x17

/* - ISO14443 A Card Combined Commands
*/
#define  GBP_GET_CARD_A_CMD           0x00
#define  GBP_GET_1ST_CARD_A_CMD       0x01
#define  GBP_REQA_SELA_CMD            0x14

/* - MIFARE Card Basic Commands
*/
#define  GBP_AUTHENTICATE_CMD         0x28
#define  GBP_READ_CMD                 0xb8
#define  GBP_WRITE_CMD                0xd8
#define  GBP_SUBVAL_CMD               0x34
#define  GBP_ADDVAL_CMD               0x36
#define  GBP_RESTORE_CMD              0xba
#define  GBP_TRANSFER_CMD             0xda

/* - MIFARE Card Combined Commands
*/
#define  GBP_CREAD_CMD                0xb8
#define  GBP_CWRITE_CMD               0xd8

#define  GBP_DEFINE_ACCESS_CMD        0x40
#define  GBP_LOADKEYS_CMD             0x24



#define  GBP_RESYNC_REQ               0xc0
#define  GBP_RESET_CMD                0xe1 /* ?? */


/* Commands from Smart Card Reader for Host ( via TRiM )
*/
#define  SC_POLL                      0x01
#define  SC_FIRMWARE                  0x02
#define  SC_READER_ID                 0x03
#define  SC_CARD_SELECTED             0x10
#define  SC_CARD_DATA_READ            0x11
#define  SC_CARD_COMPLETE             0x12

/* Responses
*/
#define  GBP_RESYNC_RESP              0xe0


#define  REQ_READERINFO               0x01
#define  READER_INFO                  0x02


/* Typedef's
*/

typedef struct
{
  byte  d_sID;
  byte  PCBn;
  byte  len;
  byte  status;
  byte  data_sz;
  byte  sw[2];
  byte  data[48];
  byte  lrc;
} GEMP_MSG;


/* Global 
*/
extern  GEMP_MSG    GEMP;


/* Prototypes
*/
void  GBP_ResyncRequest        ( void );
void  GBP_Reset                ( void );
void  GBP_RG_LED_Control       ( byte, byte, byte, byte, byte );
void  GBP_B_LED_Control        ( byte );
void  GBP_Buzzer               ( byte );
void  GBP_SetDelay             ( byte );
void  GBP_ConfigCom            ( byte );
void  GBP_ChangeMode           ( byte );
void  GBP_ReadROSFirmware      ( void );
void  GBP_ReadOROSFirmware     ( void );
void  GBP_Read_EEPROM          ( byte );
void  GBP_Write_EEPROM         ( byte, byte );
void  GBP_GetReader_ID         ( void );
void  GBP_RF_On                ( void );
void  GBP_RF_Off               ( void );
void  GBP_SetModType           ( byte );
void  GBP_ReadModType          ( void );
void  GBP_GetReaderParams      ( void );
void  GBP_ReqAll_A             ( void );
void  GBP_Req_A                ( void );
void  GBP_Anticoll             ( byte );
void  GBP_Select               ( byte );
void  GBP_HaltA                ( void );
void  GBP_Req_ATS              ( byte );
void  GBP_ProtoPara_SelReq     ( byte, byte );
void  GBP_Get_1st_CardA        ( byte );
void  GBP_Get_CardA            ( byte );
void  GBP_ReqAll_SelA          ( char*, int );
void  GBP_Authenticate         ( byte, byte, byte );
void  GBP_Read                 ( byte, byte );
void  GBP_Write                ( byte, byte );
void  GBP_SubVal               ( byte, byte, long );
void  GBP_AddVal               ( byte, byte, long );
void  GBP_Restore              ( byte, byte );
void  GBP_Transfer             ( byte, byte );
void  GBP_CRead                ( byte, byte, byte, byte );
void  GBP_CWrite               ( byte, byte, byte, byte );

void  GBP_LoadKeys             ( byte, byte, byte, byte );




void  GBP_ResyncRequest        ( void );
void  GBP_Reset                ( void );
void  GBP_1                    ( void );

void  TLP_Reset                ( void );


int   TrimSCCmd                ( int, byte, void*, int );
void  IssueTrimSC              ( void );


#endif /* __GEMP_H_ */
