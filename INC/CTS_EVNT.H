/******************************************************************************
*  Inc File:   CTS_evnt.h
*  Authored:   04/08/03, jks
*  Function:   Defines & prototypes for events for Cubic's DCU.
*  Comments:   
*
*  $Log: $
*  Revision:
*              04/08/03  jks - Initial release.
*
*                       Copyright (c) 2003  GFI Genfare
*
******************************************************************************/
#ifndef  _CTS_EVENTS_H
#define  _CTS_EVENTS_H

/* Project headers
*/
#include "gen.h"


/* Macros
*/
#define QUEUE_CTS_DCU_EVENT if( CTS_Dcu ) Queue_CTS_DCU_Event


/* Defines
*/

/* events */
#define  INITIALIZATION_COMPLETE                1
#define  CASH_DUMP_COMPLETE                     2
#define  KEY_PRESSED                            3
#define  COIN_VALIDATED                         4
#define  BILL_VALIDATED                         5
#define  COIN_REJECTED                          6
#define  BILL_REJECTED                          7
#define  TICKET_INSERTED                        8
#define  TICKET_READ                            9
#define  TICKET_ISSUE_COMPLETE                 10
#define  TICKET_CAPTURED                       11
#define  PROBE_EVENT                           12
#define  CASHBOX_EVENT                         13
#define  SECURITY_BREACH                       14
#define  ESCROW_BILL_RETURNED                  15
#define  ESCROW_BILL_BANKED                    16
#define  WARM_COLD_RESTART                     17


/* status bits */
  /* byte 0 (fbx_stat) */
#define CTS_DCU_FBX_STAT_HIBERNATE           0x01
#define CTS_DCU_FBX_STAT_CLOCK_ERROR         0x02
#define CTS_DCU_FBX_STAT_NEED_FBX_ID         0x04
#define CTS_DCU_FBX_STAT_LOW_POWER           0x08
#define CTS_DCU_FBX_STAT_CBX_PROBING         0x10
#define CTS_DCU_FBX_STAT_MAINT_JUMPER        0x20
#define CTS_DCU_FBX_STAT_UNAVAILABLE         0x40
#define CTS_DCU_FBX_STAT_OUT_OF_SERVICE      0x80
/*
#define  ALL_CTS_FBXFLAGS  ( byte )\
                           (CTS_DCU_FBX_STAT_HIBERNATE|\     
                            CTS_DCU_FBX_STAT_CLOCK_ERROR|\
                            CTS_DCU_FBX_STAT_NEED_FBX_ID|\ 
                            CTS_DCU_FBX_STAT_LOW_POWER|\ 
                            CTS_DCU_FBX_STAT_CBX_PROBING|\   
                            CTS_DCU_FBX_STAT_MAINT_JUMPER|\ 
                            CTS_DCU_FBX_STAT_UNAVAILABLE|\
                            CTS_DCU_FBX_STAT_OUT_OF_SERVICE)

*/
#define  Set_CTS_FBX_Sflag( f )        ( Cubic_DCU_status.fbx_stat |= (f) )
#define  Clr_CTS_FBX_Sflag( f )        ( Cubic_DCU_status.fbx_stat &=~(f) )
#define  Tst_CTS_FBX_Sflag( f )        ( Cubic_DCU_status.fbx_stat &  (f) )

  /* byte 1 (cbx_stat) */
#define CTS_DCU_CBX_STAT_ID_FAILURE          0x01
#define CTS_DCU_CBX_STAT_LIMIT_4             0x02
#define CTS_DCU_CBX_STAT_LIMIT_3             0x04
#define CTS_DCU_CBX_STAT_LIMIT_2             0x08
#define CTS_DCU_CBX_STAT_LIMIT_1             0x10
#define CTS_DCU_CBX_STAT_CASHBOX_REMOVED     0x20
#define CTS_DCU_CBX_STAT_DOOR_OPEN           0x40
#define CTS_DCU_CBX_STAT_DOOR_UNLOCKED       0x80
/*
#define  ALL_CTS_CBXFLAGS  ( byte )\
                           (CTS_DCU_CBX_STAT_ID_FAILURE|\
                            CTS_DCU_CBX_STAT_LIMIT_4|\
                            CTS_DCU_CBX_STAT_LIMIT_3|\
                            CTS_DCU_CBX_STAT_LIMIT_2|\
                            CTS_DCU_CBX_STAT_LIMIT_1|\
                            CTS_DCU_CBX_STAT_CASHBOX_REMOVED|\
                            CTS_DCU_CBX_STAT_DOOR_OPEN|\
                            CTS_DCU_CBX_STAT_DOOR_UNLOCKED)

*/
#define  Set_CTS_CBX_Sflag( f )        ( Cubic_DCU_status.cbx_stat |= (f) )
#define  Clr_CTS_CBX_Sflag( f )        ( Cubic_DCU_status.cbx_stat &=~(f) )
#define  Tst_CTS_CBX_Sflag( f )        ( Cubic_DCU_status.cbx_stat &  (f) )

  /* byte 2 (maint_stat) */
#define CTS_DCU_MAINT_STAT_SPARE_1           0x01
#define CTS_DCU_MAINT_STAT_SPARE_2           0x02
#define CTS_DCU_MAINT_STAT_SPARE_3           0x04
#define CTS_DCU_MAINT_STAT_SPARE_4           0x08
#define CTS_DCU_MAINT_STAT_SPARE_5           0x10
#define CTS_DCU_MAINT_STAT_UNUSED_1          0x20
#define CTS_DCU_MAINT_STAT_UNUSED_2          0x40
#define CTS_DCU_MAINT_STAT_MAINT_COVER_OPEN  0x80
/*
#define  ALL_CTS_MAINTFLAGS  ( byte )\
                             (CTS_DCU_MAINT_STAT_SPARE_1|\
                              CTS_DCU_MAINT_STAT_SPARE_2|\
                              CTS_DCU_MAINT_STAT_SPARE_3|\
                              CTS_DCU_MAINT_STAT_SPARE_4|\
                              CTS_DCU_MAINT_STAT_SPARE_5|\
                              CTS_DCU_MAINT_STAT_UNUSED_1|\
                              CTS_DCU_MAINT_STAT_UNUSED_2|\
                              CTS_DCU_MAINT_STAT_MAINT_COVER_OPEN)
*/
#define  Set_CTS_LID_Sflag( f )        ( Cubic_DCU_status.lid_stat |= (f) )
#define  Clr_CTS_LID_Sflag( f )        ( Cubic_DCU_status.lid_stat &=~(f) )
#define  Tst_CTS_LID_Sflag( f )        ( Cubic_DCU_status.lid_stat &  (f) )

  /* byte 3 (cm_stat) */
#define CTS_DCU_CM_STAT_SPARE_1              0x01
#define CTS_DCU_CM_STAT_SPARE_2              0x02
#define CTS_DCU_CM_STAT_SPARE_3              0x04
#define CTS_DCU_CM_STAT_SPARE_4              0x08
#define CTS_DCU_CM_STAT_COIN_BYPASS          0x10
#define CTS_DCU_CM_STAT_UNUSED_1             0x20
#define CTS_DCU_CM_STAT_UNUSED_2             0x40
#define CTS_DCU_CM_STAT_COMM_FAILURE         0x80
/*
#define  ALL_CTS_CMFLAGS  ( byte )\
                          (CTS_DCU_CM_STAT_SPARE_1|\     
                           CTS_DCU_CM_STAT_SPARE_2|\
                           CTS_DCU_CM_STAT_SPARE_3|\ 
                           CTS_DCU_CM_STAT_SPARE_4|\ 
                           CTS_DCU_CM_STAT_COIN_BYPASS|\ 
                           CTS_DCU_CM_STAT_UNUSED_1|\ 
                           CTS_DCU_CM_STAT_UNUSED_2|\ 
                           CTS_DCU_CM_STAT_COMM_FAILURE)
*/
#define  Set_CTS_CM_Sflag( f )        ( Cubic_DCU_status.cm_stat |= (f) )
#define  Clr_CTS_CM_Sflag( f )        ( Cubic_DCU_status.cm_stat &=~(f) )
#define  Tst_CTS_CM_Sflag( f )        ( Cubic_DCU_status.cm_stat &  (f) )

  /* byte 4 (bv_stat) */
#define CTS_DCU_BV_STAT_SPARE_1              0x01
#define CTS_DCU_BV_STAT_SPARE_2              0x02
#define CTS_DCU_BV_STAT_SPARE_3              0x04
#define CTS_DCU_BV_STAT_DISABLED             0x08
#define CTS_DCU_BV_STAT_ESCROW_MODE          0x10
#define CTS_DCU_BV_STAT_RECLASSIFY           0x20
#define CTS_DCU_BV_STAT_BILL_JAM             0x40
#define CTS_DCU_BV_STAT_COMM_FAILURE         0x80
/*
#define  ALL_CTS_BVFLAGS  ( byte )\
                          (CTS_DCU_BV_STAT_SPARE_1|\     
                           CTS_DCU_BV_STAT_SPARE_2|\
                           CTS_DCU_BV_STAT_SPARE_3|\   
                           CTS_DCU_BV_STAT_SPARE_4|\ 
                           CTS_DCU_BV_STAT_ESCROW_MODE|\ 
                           CTS_DCU_BV_STAT_RECLASSIFY|\
                           CTS_DCU_BV_STAT_BILL_JAM|\
                           CTS_DCU_BV_STAT_COMM_FAILURE)
*/
#define  Set_CTS_BV_Sflag( f )        ( Cubic_DCU_status.bv_stat |= (f) )
#define  Clr_CTS_BV_Sflag( f )        ( Cubic_DCU_status.bv_stat &=~(f) )
#define  Tst_CTS_BV_Sflag( f )        ( Cubic_DCU_status.bv_stat &  (f) )

  /* byte 5 (tpu_stat) */
#define CTS_DCU_TPU_STAT_BLOCKED_SENSOR      0x01
#define CTS_DCU_TPU_STAT_TKT_STOCK_OUT       0x02
#define CTS_DCU_TPU_STAT_TKT_STOCK_LOW       0x04
#define CTS_DCU_TPU_STAT_TKT_PROCESSING      0x08
#define CTS_DCU_TPU_STAT_BYPASS              0x10
#define CTS_DCU_TPU_STAT_AUTO_ACCEPT         0x20
#define CTS_DCU_TPU_STAT_TKT_JAM             0x40
#define CTS_DCU_TPU_STAT_COMM_FAILURE        0x80
/*
#define  ALL_CTS_TPUFLAGS  ( byte )\
                          (CTS_DCU_TPU_STAT_BLOCKED_SENSOR|\
                           CTS_DCU_TPU_STAT_TKT_STOCK_OUT|\
                           CTS_DCU_TPU_STAT_TKT_STOCK_LOW|\
                           CTS_DCU_TPU_STAT_UNUSED_1|\
                           CTS_DCU_TPU_STAT_BYPASS|\    
                           CTS_DCU_TPU_STAT_AUTO_ACCEPT|\   
                           CTS_DCU_TPU_STAT_TKT_JAM|\ 
                           CTS_DCU_TPU_STAT_COMM_FAILURE)
*/

#define  Set_CTS_TPU_Sflag( f )        ( Cubic_DCU_status.tpu_stat |= (f) )
#define  Clr_CTS_TPU_Sflag( f )        ( Cubic_DCU_status.tpu_stat &=~(f) )
#define  Tst_CTS_TPU_Sflag( f )        ( Cubic_DCU_status.tpu_stat &  (f) )

  /* byte 6 (pd_stat) */
#define  Set_CTS_PD_Sflag( f )        ( Cubic_DCU_status.pd_stat |= (f) )
#define  Clr_CTS_PD_Sflag( f )        ( Cubic_DCU_status.pd_stat &=~(f) )
#define  Tst_CTS_PD_Sflag( f )        ( Cubic_DCU_status.pd_stat &  (f) )


/* coins */
#define CTS_PENNY                0
#define CTS_NICKEL               1
#define CTS_DIME                 2
#define CTS_QUARTER              3
#define CTS_HALF_DOLLAR          4
#define CTS_DOLLAR_COIN          5
#define CTS_065_TOKEN            6
#define CTS_078_TOKEN            7
#define CTS_090_TOKEN            8
#define CTS_100_TOKEN            9
#define CTS_112_TOKEN           10
#define CTS_125_TOKEN           11
#define CTS_RESERVED_COIN       12
#define CTS_UNCLASSIFIED_COIN   13

/* bills */
#define CTS_UNCLASSIFIED_BILL    0
#define CTS_1_DOLLAR_BILL        1
#define CTS_2_DOLLAR_BILL        2
#define CTS_5_DOLLAR_BILL        3
#define CTS_10_DOLLAR_BILL       4
#define CTS_20_DOLLAR_BILL       5
#define CTS_50_DOLLAR_BILL       6
#define CTS_100_DOLLAR_BILL      7
#define CTS_TICKET               8
#define CTS_RESERVED_BILL_1      9 /* 3.5" Ticket (Added for Marietta, GA) */
#define CTS_RESERVED_BILL_2     10 /* 4.0" Ticket (Added for Marietta, GA) */
#define CTS_RESERVED_BILL_3     11 /* 4.5" Ticket (Added for Marietta, GA) */
#define CTS_RESERVED_BILL_4     12 /* 5.0" Ticket (Added for Marietta, GA) */


/* Stuctures
*/
typedef struct
{
  byte      fbx_stat;        /* Byte 00
                                bit 7 - In/Out of service ( 0 = OOS          )
                                bit 6 - UNUSED
                                bit 5 - Maint. jumper     ( 0 = out          )
                                bit 4 - Probing active.   ( 0 = Inactive     )
                                bit 3 - Low Power         ( 0 = Power OK     )
                                bit 2 - Memory Clr        ( 1 = Need FBX Id  )
                                bit 1 - Clock error       ( 0 = OK           )
                                bit 0 - Spare
                                                                            */
  byte      cbx_stat;        /* Byte 01
                                bit 7 - UNUSED
                                bit 6 - Cashbox Door      ( 1 = Open         )
                                bit 5 - Cashbox ( 1 = removed, 0 = inserted  )
                                bit 4 - Cashbox Limit #1 25%  ( 1 = Reached  )
                                bit 3 - Cashbox Limit #2 50%  ( 1 = Reached  )
                                bit 2 - Cashbox Limit #3 75%  ( 1 = Reached  )
                                bit 1 - Cashbox Limit #4 100% ( 1 = Reached  )
                                bit 0 - Cashbox ID        ( 1 = Failure      )
                                                                            */
  byte      lid_stat;        /* Byte 02
                                bit 7 - Maintenance Cover ( 1 = Open         )
                                bit 6 - UNUSED
                                bit 5 - UNUSED
                                bit 4-0 Spare
                                                                            */
  byte      cm_stat;         /* Byte 03
                                bit 7 - Coin Validator Comm ( 1 = Failure    )
                                bit 6 - Unused
                                bit 5 - Unused
                                bit 4 - Coin Bypass       ( 1 = Activated    )
                                bit 3-0 Reserved
                                                                            */
  byte      bv_stat;         /* Byte 04
                                bit 7 - BV Communications ( 1 = Failure      )
                                bit 6 - bill jam          ( 1 = bill jam     )
                                bit 5 - Bill Override     ( 1 = Active       )
                                bit 4 - escrow mode       ( 1 = escrow mode  )
                                bit 3 - disable bill mech ( 1 = disabled     )
                                bit 2-0 Reserved
                                                                            */
  byte      tpu_stat;        /* Byte 05
                                bit 7 - Ticket Communications ( 1 = Failure  )
                                bit 6 - Ticket Jam        ( 1 = Jammed       )
                                bit 5 - UNUSED
                                bit 4 - UNUSED
                                bit 3 - UNUSED
                                bit 2 - Ticket Stock      ( 1 = LOW, 0 = OK  )
                                bit 1 - Ticket Stock      ( 1 = OUT, 0 = OK  )
                                bit 0 - Spare
                                                                            */
  byte      pd_stat;         /* Byte 06
                                bit 7 - UNUSED
                                bit 6 - UNUSED
                                bit 5-0 Spare
                                                                            */
  byte      spare[9];
} STATUS;


/* Global Data
*/
extern STATUS      Cubic_DCU_status;


/* Prototypes
*/
extern void Queue_CTS_DCU_Event ( byte, byte*, byte );


#endif /*_CTS_EVENTS_H*/
