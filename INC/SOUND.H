/*----------------------------------------------------------------------------\
|  Src File:   sound.h                                                        |
|  Authored:   10/24/94, tgh                                                  |
|  Function:   Defines & prototypes for sound functions.                      |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   10/24/94, tgh  -  Initial release.                             |
|                                                                             |
|           Copyright (c) 1993, 1994 GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
#ifndef  _SOUND_H
#define  _SOUND_H

#include "gen.h"
#include "port.h"
#include "timers.h"

#define base               0 
#define beep               Sound( base+0  )
#define chirp              Sound( base+1  )
#define worble             Sound( base+2  )
#define PayAmount          Sound( base+3  )
#define PressChangeKey     Sound( base+4  )
#define TakeXferSound      Sound( base+5  )
#define XferAcceptSound    Sound( base+6  )
#define XferInvalidSound   Sound( base+7  )
#define Fare_Deducted      Sound( base+8  )
#define ShortFareOnCard    Sound( base+9  )
#define InvalidCardSound   Sound( base+10 )
#define RemainingVal       Sound( base+11 )
#define TouchCardAgain     Sound( base+12 )
#define NoValue            Sound( base+13 )
#define CheckDenomination  Sound( base+14 )
#define Coin_Return_Sound  Sound( base+15 )
#define ChangeSound        Sound( base+16 )
#define IncludesXfer       Sound( base+17 )
#define TakeReceiptSound   Sound( base+18 )
#define ShowID             Sound( base+19 )
#define DoubleBeep         Sound( base+20 )
#define TrippleBeep        Sound( base+21 )
#define ShortChirp         Sound( base+22 )
#define Shortworble        Sound( base+23 )
#define SoundEnd           24

/*  old sound 
#define beep               Sound( base+0  )
#define chirp              Sound( base+1  )
#define worble             Sound( base+2  )
#define TakeXferSound      Sound( base+6  )
#define XferAcceptSound    Sound( base+7  )
#define XferInvalidSound   Sound( base+8  )
#define InvalidCardSound   Sound( base+11 )
#define ChangeSound        Sound( base+17 )
#define TakeReceiptSound   Sound( base+19 )
#define Bevus              Sound( base+20 )
#define SoundEnd           21
*/ 

/* prototype */

void  BeepOnce    ( int  );
void CheckSound   ( void );

#endif /*_SOUND_H*/
